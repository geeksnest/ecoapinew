<?php
namespace Controllers;
use \Models\Image as Image;
use \Models\Album  as Album;
use \Models\Slider  as Slider;
use \Models\Banneralbum  as Banneralbum;
use \Models\Banner  as Banner;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
class ViewalbumController extends \Phalcon\Mvc\Controller
{
    public function addAlbumAction() {
        var_dump($_POST);
        $album = Album::findFirst("album_name='" . $_POST['albumname'] ."'");
        if($album){
            $generateid = md5(uniqid(rand(), true));
            // $max = Slider::maximum("album_name='" . $_POST['albumname'] ."'");
            $max = Slider::maximum(
                array(
                    "column"     => "sort",
                    "conditions" => "album_name = '". $_POST['albumname'] ."'"
                    )
                );
            $imgUpload = new Slider();
            $imgUpload->assign(array(
                'imgID'=>$generateid,
                'description'=>'Description Here',
                'title'=>'Title Here',
                'album_name'=>$_POST['albumname'],
                'foldername'=>$_POST['foldername'],
                'album_id'=>$album->album_id,
                'img'=>$_POST['img'],
                'sort'=>$max + 1,
                'date_created'=> date('Y-m-d'),
                'date_updated'=> date("Y-m-d H:i:s"),
                'titlefontsize'=> 45,
                'descriptionfontsize'=> 18,
                'showtext'=> 'true',
                'tbgcolor'=> 'rgba(114, 187, 76, 0.7)',
                'tfcolor'=> '#ffffff',
                'dfcolor'=> '#ffffff',
                'status'=> 1
                ));
            if (!$imgUpload->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $imgUpload->getMessages()]);
            }else{
                $data['success'] = "Image Uploaded.";
                echo json_encode($data);
            }

        }else{

            $albumID = md5(uniqid(rand(), true));
            $newAlbum = new Album();
            $newAlbum->assign(array(
                'album_id'=> $albumID,
                'album_name'=> $_POST['albumname'],
                'foldername'=> $_POST['foldername'],
                'date_created'=> date('Y-m-d H:i:s')
                ));

            if ($newAlbum->save()) {
                $generateid = md5(uniqid(rand(), true));
                $imgUpload = new Slider();
                $imgUpload->assign(array(
                    'imgID'=>$generateid,
                    'description'=>'Description Here',
                    'title'=>'Title Here',
                    'album_name'=>$_POST['albumname'],
                    'foldername'=>$_POST['foldername'],
                    'album_id'=>$albumID,
                    'img'=>$_POST['img'],
                    'sort'=>$_POST['filecount'],
                    'date_created'=> date('Y-m-d'),
                    'date_updated'=> date("Y-m-d H:i:s"),
                    'titlefontsize'=> 45,
                    'descriptionfontsize'=> 18,
                    'showtext'=> 'true',
                    'tbgcolor'=> 'rgba(114, 187, 76, 0.7)',
                    'tfcolor'=> '#ffffff',
                    'dfcolor'=> '#ffffff',
                    'status'=> 1
                    ));
                if (!$imgUpload->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $imgUpload->getMessages()]);
                }else{
                    $data['success'] = "Image Uploaded.";
                    echo json_encode($data);
                }
            }else{
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $newAlbum->getMessages()]);
            }




        }


    }



    public function banneraddrAlbumAction() {
        var_dump($_POST);
        $album = Banneralbum::findFirst("album_name='" . $_POST['albumname'] ."'");
        if($album){
            $generateid = md5(uniqid(rand(), true));
            // $max = Slider::maximum("album_name='" . $_POST['albumname'] ."'");
            $max = Banner::maximum(
                array(
                    "column"     => "sort",
                    "conditions" => "album_name = '". $_POST['albumname'] ."'"
                    )
                );
            $imgUpload = new Banner();
            $imgUpload->assign(array(
                'imgID'=>$generateid,
                'description'=>'Description Here',
                'title'=>'Title Here',
                'album_name'=>$_POST['albumname'],
                'foldername'=>$_POST['foldername'],
                'album_id'=>$album->album_id,
                'img'=>$_POST['img'],
                'sort'=>$max + 1,
                'date_created'=> date('Y-m-d'),
                'date_updated'=> date("Y-m-d H:i:s"),
                'titlefontsize'=> 45,
                'descriptionfontsize'=> 18,
                'showtext'=> 'true',
                'tbgcolor'=> 'rgba(114, 187, 76, 0.7)',
                'tfcolor'=> '#ffffff',
                'dfcolor'=> '#ffffff',
                'status'=> 1
                ));
            if (!$imgUpload->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $imgUpload->getMessages()]);
            }else{
                $data['success'] = "Image Uploaded.";
                echo json_encode($data);
            }

        }else{

            $albumID = md5(uniqid(rand(), true));
            $newAlbum = new Banneralbum();
            $newAlbum->assign(array(
                'album_id'=> $albumID,
                'album_name'=> $_POST['albumname'],
                'foldername'=> $_POST['foldername'],
                'date_created'=> date('Y-m-d H:i:s')
                ));

            if ($newAlbum->save()) {
                $generateid = md5(uniqid(rand(), true));
                $imgUpload = new Banner();
                $imgUpload->assign(array(
                    'imgID'=>$generateid,
                    'description'=>'Description Here',
                    'title'=>'Title Here',
                    'album_name'=>$_POST['albumname'],
                    'foldername'=>$_POST['foldername'],
                    'album_id'=>$albumID,
                    'img'=>$_POST['img'],
                    'sort'=>$_POST['filecount'],
                    'date_created'=> date('Y-m-d'),
                    'date_updated'=> date("Y-m-d H:i:s"),
                    'titlefontsize'=> 45,
                    'descriptionfontsize'=> 18,
                    'showtext'=> 'true',
                    'tbgcolor'=> 'rgba(114, 187, 76, 0.7)',
                    'tfcolor'=> '#ffffff',
                    'dfcolor'=> '#ffffff',
                    'status'=> 1
                    ));
                if (!$imgUpload->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $imgUpload->getMessages()]);
                }else{
                    $data['success'] = "Image Uploaded.";
                    echo json_encode($data);
                }
            }else{
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $newAlbum->getMessages()]);
            }




        }


    }




    public function addAlbumItemAction() {
        var_dump($_POST);
        $album = Album::findFirst("album_name='" . $_POST['album_name'] ."'");
        if($album){
            $generateid = md5(uniqid(rand(), true));
            $max = Slider::maximum(
                array(
                    "column"     => "sort",
                    "conditions" => "album_name = '". $_POST['album_name'] ."'"
                    )
                );
            $imgUpload = new Slider();
            $imgUpload->assign(array(
                'imgID'=>$generateid,
                'description'=>'Description Here',
                'title'=>'Title Here',
                'album_name'=>$_POST['album_name'],
                'foldername'=>$_POST['foldername'],
                'album_id'=>$album->album_id,
                'img'=>$_POST['img'],
                'sort'=>$max + 1,
                'date_created'=> date('Y-m-d'),
                'date_updated'=> date("Y-m-d H:i:s"),
                'titlefontsize'=> 45,
                'descriptionfontsize'=> 18,
                'showtext'=> 'true',
                'tbgcolor'=> 'rgba(114, 187, 76, 0.7)',
                'tfcolor'=> '#ffffff',
                'dfcolor'=> '#ffffff',
                'status'=> 1
                ));
            if (!$imgUpload->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $imgUpload->getMessages()]);
            }else{
                $data['success'] = "Image Uploaded.";
                echo json_encode($data);
            }

        }


    }


    public function banneraddAlbumItemAction() {

        $album = Banneralbum::findFirst("album_name='" . $_POST['album_name'] ."'");
        if($album){
            $generateid = md5(uniqid(rand(), true));
            $max = Banner::maximum(
                array(
                    "column"     => "sort",
                    "conditions" => "album_name = '". $_POST['album_name'] ."'"
                    )
                );
            $imgUpload = new Banner();
            $imgUpload->assign(array(
                'imgID'=>$generateid,
                'description'=>'Description Here',
                'title'=>'Title Here',
                'album_name'=>$_POST['album_name'],
                'foldername'=>$_POST['foldername'],
                'album_id'=>$album->album_id,
                'img'=>$_POST['img'],
                'sort'=>$max + 1,
                'date_created'=> date('Y-m-d'),
                'date_updated'=> date("Y-m-d H:i:s"),
                'titlefontsize'=> 45,
                'descriptionfontsize'=> 18,
                'showtext'=> 'true',
                'tbgcolor'=> 'rgba(114, 187, 76, 0.7)',
                'tfcolor'=> '#ffffff',
                'dfcolor'=> '#ffffff',
                'status'=> 1
                ));
            if (!$imgUpload->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $imgUpload->getMessages()]);
            }else{
                $data['success'] = "Image Uploaded.";
                echo json_encode($data);
            }

        }


    }


    public function albumslistAction($all) {

        $album = Album::find(array("order" => "main DESC"));

        foreach ($album as $album) {

            $db = \Phalcon\DI::getDefault()->get('db');
            $conditions = $db->prepare("SELECT * FROM slider WHERE album_id = '".$album->album_id."' AND sort=1");
            $conditions->execute();
            $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);

            $data[] = array(
                'num'=>$album->num,
                'album_name'=>$album->album_name,
                'album_id'=>$album->album_id,
                'main'=>$album->main,
                'date_created' => $album->date_created,
                'item' => $item
                );
        }
        echo json_encode($data);


    }


    public function banneralbumslistAction($all) {

        $album = Banneralbum::find(array("order" => "num DESC"));

        foreach ($album as $album) {

            $db = \Phalcon\DI::getDefault()->get('db');
            $conditions = $db->prepare("SELECT * FROM banner WHERE album_id = '".$album->album_id."' AND sort=1");
            $conditions->execute();
            $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);

            $data[] = array(
                'num'=>$album->num,
                'album_name'=>$album->album_name,
                'album_id'=>$album->album_id,
                'main'=>$album->main,
                'date_created' => $album->date_created,
                'item' => $item
                );
        }
        echo json_encode($data);


    }


    public function albumsimagesAction($albumid) {

        $album = Album::findFirst("album_id='" . $albumid ."'");

        $data = array();

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM slider WHERE album_id = '".$albumid."' ORDER BY sort ASC");
        $conditions->execute();
        $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        $data = array(
            'num'=>$album->num,
            'album_name'=>$album->album_name,
            'album_id'=>$album->album_id,
            'foldername'=>$album->foldername,
            'date_created' => $album->date_created,
            'itemcount' => count($item),
            'item' => $item
            );
        echo json_encode($data);


    }


    public function banneralbumsimagesAction($albumid) {

        $album = Banneralbum::findFirst("album_id='" . $albumid ."'");

        $data = array();

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM banner WHERE album_id = '".$albumid."' ORDER BY sort ASC");
        $conditions->execute();
        $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        $data = array(
            'num'=>$album->num,
            'album_name'=>$album->album_name,
            'album_id'=>$album->album_id,
            'foldername'=>$album->foldername,
            'date_created' => $album->date_created,
            'itemcount' => count($item),
            'item' => $item
            );
        echo json_encode($data);


    }


    public function sortSliderAction($imgID,$sort,$album_id) {


        $image = Slider::findFirst("sort='" . $sort ."' AND album_id='" . $album_id ."'");
        $image2 = Slider::findFirst("imgID='" . $imgID ."'");
        if($image){

            $image->sort = $image2->sort;

            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $image2->sort = $sort;

                if (!$image2->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";

                    $image3 = Slider::findFirst("sort = 0 AND album_id='" . $album_id ."'");
                    $count = Slider::find("album_id='" . $album_id ."'");
                    $total = count($count);
                    var_dump(count($count));
                    if($image3){
                        $image3->sort = 5;
                        if (!$image3->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                        } else {
                            $data['success'] = "Success";
                        }
                    }

                }
            }

        }

    }


    public function sortBannerAction($imgID,$sort,$album_id) {


        $image = Banner::findFirst("sort='" . $sort ."' AND album_id='" . $album_id ."'");
        $image2 = Banner::findFirst("imgID='" . $imgID ."'");
        if($image){

            $image->sort = $image2->sort;

            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $image2->sort = $sort;

                if (!$image2->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";

                    $image3 = Banner::findFirst("sort = 0 AND album_id='" . $album_id ."'");
                    $count = Banner::find("album_id='" . $album_id ."'");
                    $total = count($count);
                    var_dump(count($count));
                    if($image3){
                        $image3->sort = 5;
                        if (!$image3->save()) {
                            $data['error'] = "Something went wrong saving the data, please try again.";
                        } else {
                            $data['success'] = "Success";
                        }
                    }

                }
            }

        }

    }



    public function editImageAction($imgID) {

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM slider WHERE imgID = '".$imgID."'");
        $conditions->execute();
        $data = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);
    }



    public function bannereditImageAction($imgID) {

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM banner WHERE imgID = '".$imgID."'");
        $conditions->execute();
        $data = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);
    }

    public function updateImageAction() {

        if($_POST){
            $image = Slider::findFirst("imgID='" . $_POST['imgID'] ."'");
            $image->title = $_POST['title'];
            $image->description = $_POST['description'];
            $image->linkname = $_POST['linkname'];
            $image->linkpath = $_POST['linkpath'];
            $image->titlefontsize = $_POST['titlefontsize'];
            $image->descriptionfontsize = $_POST['descriptionfontsize'];
            $image->showtext = $_POST['showtext'];
            $image->tbgcolor = $_POST['tbgcolor'];
            $image->tfcolor = $_POST['tfcolor'];
            $image->dfcolor = $_POST['dfcolor'];
            $image->img = $_POST['img'];


            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
    }

    public function bannerupdateImageAction() {

        if($_POST){
            $image = Banner::findFirst("imgID='" . $_POST['imgID'] ."'");
            $image->title = $_POST['title'];
            $image->description = $_POST['description'];
            $image->linkname = $_POST['linkname'];
            $image->linkpath = $_POST['linkpath'];
            $image->titlefontsize = $_POST['titlefontsize'];
            $image->descriptionfontsize = $_POST['descriptionfontsize'];
            $image->showtext = $_POST['showtext'];
            $image->tbgcolor = $_POST['tbgcolor'];
            $image->tfcolor = $_POST['tfcolor'];
            $image->dfcolor = $_POST['dfcolor'];
            $image->img = $_POST['img'];


            if (!$image->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }

        }
    }

    public function setMainSliderAction($album_id) {

        $album2 = Album::findFirst("main = 1");
        if($album2){

            $album2->main = 0;

            if (!$album2->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $album = Album::findFirst("album_id='" . $album_id ."'");
                if($album2){

                    $album->main = 1;
                    if (!$album->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";
                    }
                }
            }
        }

    }

    public function setMainBannerAction($album_id) {

        $album2 = Banneralbum::findFirst("main = 1");
        if($album2){

            $album2->main = 0;

            if (!$album2->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";

                $album = Banneralbum::findFirst("album_id='" . $album_id ."'");
                if($album2){

                    $album->main = 1;
                    if (!$album->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";
                    }
                }
            }
        }

    }

    public function deleteImageAction($imgID) {

        $dltPhoto = Slider::findFirst("imgID='" . $imgID ."'");
        $album_id = $dltPhoto->album_id;
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');

            }

            $images = Slider::find(array("album_id='" . $album_id ."'","order" => "sort ASC"));

            if ($images) {
                $x = 1;

                foreach($images as $images){
                    $images->sort = $x;
                    $x++;

                    if ($images->save()) {
                        $data = array('success' => 'Deleted');
                    }
                }
            }
        }
        echo json_encode($data);

    }

    public function bannerdeleteImageAction($imgID) {

        $dltPhoto = Banner::findFirst("imgID='" . $imgID ."'");
        $album_id = $dltPhoto->album_id;
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');

            }

            $images = Banner::find(array("album_id='" . $album_id ."'","order" => "sort ASC"));

            if ($images) {
                $x = 1;

                foreach($images as $images){
                    $images->sort = $x;
                    $x++;

                    if ($images->save()) {
                        $data = array('success' => 'Deleted');
                    }
                }
            }
        }
        echo json_encode($data);

    }

    public function deleteAlbumAction($album_id) {

        $dltAlbum = Album::findFirst("album_id='" . $album_id ."'");
        $data = array('error' => 'Not Found');
        if ($dltAlbum) {
            if($dltAlbum->delete()){
                $data = array('success' => 'Photo has Been deleted');

            }
        }

        $dltImages = Slider::find("album_id='" . $album_id ."'");
        if ($dltImages) {
            foreach($dltImages as $dltImages){

                if ($dltImages->delete()) {
                    $data = array('success' => 'Deleted');
                }
            }
        }

        echo json_encode($data);

    }

    public function bannerdeleteAlbumAction($album_id) {

        $dltAlbum = Banneralbum::findFirst("album_id='" . $album_id ."'");
        $data = array('error' => 'Not Found');
        if ($dltAlbum) {
            if($dltAlbum->delete()){
                $data = array('success' => 'Photo has Been deleted');

            }
        }

        $dltImages = Banner::find("album_id='" . $album_id ."'");
        if ($dltImages) {
            foreach($dltImages as $dltImages){

                if ($dltImages->delete()) {
                    $data = array('success' => 'Deleted');
                }
            }
        }

        echo json_encode($data);

    }

    public function sliderSetStatusAction($imgID) {

        $setStatus = Slider::findFirst("imgID='" . $imgID ."'");
        if ($setStatus) {

            if($setStatus->status == 1){
                $setStatus->status = 0;
            }else{
                $setStatus->status = 1;
            }

            if ($setStatus->save()) {
                $data = array('success' => 'Set Status');
            }
        }
        echo json_encode($data);

    }

    public function bannerSetStatusAction($imgID) {

        $setStatus = Banner::findFirst("imgID='" . $imgID ."'");
        if ($setStatus) {

            if($setStatus->status == 1){
                $setStatus->status = 0;
            }else{
                $setStatus->status = 1;
            }

            if ($setStatus->save()) {
                $data = array('success' => 'Set Status');
            }
        }
        echo json_encode($data);

    }


    public function albuminfoAction($albumid) {

        $album = Album::findFirst("album_id='" . $albumid."'");
        $album_info = array();
        if ($album) {
            $album_info = array(
                'album_name' => $album ->album_name,
                'album_id' => $album ->album_id
            );
        }
        echo json_encode($album_info);
    }



    public function mainslideAction() {        

        $album = Album::findFirst('main=1');

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM slider WHERE album_id = '".$album->album_id."' AND status = 1 ORDER BY sort ASC");
        $conditions->execute();
        $item = $conditions->fetchAll(\PDO::FETCH_ASSOC);
        
        echo json_encode($item);
    }

    public function iCaresliderAction() {        
        $app = new CB();

        $album = Album::findFirst('main=5');
        $conditions = "SELECT * FROM slider WHERE album_id = '".$album->album_id."' AND status = 1 ORDER BY sort ASC";        
        $item = $app->dbSelect($conditions);
        
        echo json_encode($item);
    }



    public function mainbannersAction() {

        $album = Banneralbum::findFirst('main=1');
        $data = array();
        if ($album) {
            $image = Banner::find(array("album_id='".$album->album_id."' AND status = 1", "order" => "sort ASC"));
            $banners = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'banners' => $banners
                );
        }
        echo json_encode($data);
    }


    public function newsBannerAction() {

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM banner WHERE album_id = 'newsbanner'");
        $conditions->execute();
        $data = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);
    }


    public function projBannerAction() {

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM banner WHERE album_id = 'projbanner'");
        $conditions->execute();
        $data = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);
    }

    public function medialibraryBannerAction() {

        $db = \Phalcon\DI::getDefault()->get('db');
        $conditions = $db->prepare("SELECT * FROM banner WHERE album_id = 'medialibrarybanner'");
        $conditions->execute();
        $data = $conditions->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($data);
    }

}

