<?php
namespace Controllers;
use \Models\Settings as Settings;
use \Models\Gscript as Gscript;
use PHPMailer as PHPMailer;
class SettingsController extends \Phalcon\Mvc\Controller{

    public function managesettingsAction(){
       $data = array();
       $setting = Settings::findFirst("id=" . 1);
         if ($setting) {
            $setting = array(                
                'id' => $setting->id,
                'value1' =>$setting->value1,
                'value2' =>$setting->value2,
                'value3' =>$setting->value3
                );
        }
        echo json_encode($setting); 

    }

    public function maintenanceAction($id) {
     $data = array();
           $setting = Settings::findFirst("id=" . $id);
             if ($setting) {
            $setting = array(                
                'id' => $setting->id,
                'value1' =>$setting->value1,
                'value2' =>$setting->value2,
                'value3' =>$setting->value3
                );
        }

        echo json_encode($setting);  


    }

    public function maintenanceonAction() {

        
        date_default_timezone_set('America/Los_Angeles');
        $date = gmdate('Y m d H i s');
                        
        $date_arr= explode(" ", $date);
        
        
        if ($date_arr[3] == 23){

        $add = 0 + $_POST['maintenance_time'];
        }
        else{        
        $addchk = $date_arr[3] + $_POST['maintenance_time'];
        if($addchk > 23){
            $addchk2 = $addchk - 24;
            $add = $addchk2 + $_POST['maintenance_time'];
            $addday = $date_arr[2] +1;
        }
        else{
            $add = $date_arr[3] + $_POST['maintenance_time'];
            $addday = $date_arr[2];
        }
    }

    $expiretime = $date_arr[0] ."-". $date_arr[1] ."-". $addday ." ". $add .":". $date_arr[4] .":". $date_arr[5];

        var_dump($_POST);
        $data = array(); 

    $setting = Settings::findFirst("id=" . 1);
        
            $setting->value1 = 1;
            $setting->value2 = $_POST['maintenance_msg'];
            $setting->value3 = $_POST['maintenance_time'];
            $setting->value4 = $expiretime;
            $setting->value5 = date('Z');
            
                
        if (!$setting->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";                
                }  
        echo json_encode($setting);    
      

    }

    public function maintenanceoffAction() {
        
        var_dump($_POST);
        $data = array(); 
        $setting = Settings::findFirst("id=" . 1);
            
                $setting->value1 = 0;

        if (!$setting->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";                
                }  
        echo json_encode($setting);   

    }

    public function scriptAction(){
        $findid = Gscript::findFirst('id=1');
        if($findid){
            $findid->gscript = $_POST['gscript'];
            if (!$findid->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }else{
                $data['success'] = "Success";
            }
        }else{
            $scipt = new Gscript();
            $scipt->assign(array( 'gscript' => $_POST['gscript'] ));
            if (!$scipt->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
    }

    public function loadscriptAction(){
        $findid = Gscript::findFirst('id=1');
        $data = array();
        if ($findid) {            
            $data = array(
                'id' => $findid->id,
                'gscript' => $findid->gscript
                );
        }
        echo json_encode($data);        
    }

    public function displaytAction() {
        $script = Gscript::find();
        echo  $info= json_encode($script->toArray(), JSON_NUMERIC_CHECK);
    }

    public function clienttimezone($tztztz) {
        
        var_dump($_POST);
        $data = array(); 
        $setting = Settings::findFirst("id=" . 1);
            
                $setting->value1 = $tztztz;

        if (!$setting->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";                
                }  
        echo json_encode($setting);   

    }



}

