<?php
namespace Controllers;

use \Models\Vendorlog as Vendorlog;
use \Models\Donationlog as Donationlog;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Controllers\ControllerBase as CB;

class VendorController extends \Phalcon\Mvc\Controller
{

     public function SeattlevendorlistAction($num, $page, $keyword, $sortpaytype, $sort, $sortto, $listIn) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        $totalvendors = Vendorlog::count(array("distinct" => "orgname"));
        $totalamount = Vendorlog::sum(array("column" => "amount"));
        $totalcollections = number_format($totalamount, 2, '.', '');

        if ($keyword == 'null' || $keyword == 'undefined') {
            if($sortpaytype == 'all' || $sortpaytype == 'undefined'){

                $conditions = "SELECT * FROM vendorlog ";

            } else{

                $conditions = "SELECT * FROM vendorlog WHERE paymentmode LIKE '" . $sortpaytype . "' ";

            }
        } else {

            if($sortpaytype == 'all' || $sortpaytype == 'undefined'){

                $conditions = "SELECT * FROM vendorlog WHERE orgname LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%'";
                $conditions .= " OR transactionId LIKE '" . $keyword . "' OR amount LIKE '" . $keyword . "' ";

            }else{

                $conditions = "SELECT * FROM vendorlog WHERE orgname LIKE '%" . $keyword . "%' OR email LIKE '%" . $keyword . "%'";
                $conditions .= " OR transactionId LIKE '" . $keyword . "' OR amount LIKE '" . $keyword . "' AND paymentmode LIKE '" . $sortpaytype . "' ";

            }
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count),
            'totalvendors' => $totalvendors,
            'totalcollections' => $totalcollections

            ));

    }

    public function viewvendorAction($vendorid) {

        $vendor = Vendorlog::findFirst('id="' . $vendorid . '"');
        $data = array();

        if ($vendor) {

        $data = array(
            'id' => $vendor->id,
            'transactionId' => $vendor->transactionId,
            'orgname' => $vendor->orgname,
            'expooffer' => $vendor->expooffer,
            'contactperson' => $vendor->contactperson,
            'phone' => $vendor->phone,
            'email' => $vendor->email,
            'address' => $vendor->address,
            'table' => $vendor->tables,
            'electricity' => $vendor->electricity,
            'totalpayment' => number_format($vendor->totalpayment, 2, '.', ''),
            'paymentmethod' => $vendor->paymentmethod,
            'amount' => number_format($vendor->amount, 2, '.', ''),
            'paymentmode' => $vendor->paymentmode,
            'forcheckmode' => $vendor->forcheckmode,
            'ccemail' => $vendor->ccemail,
            'billinginfofname' => $vendor->billinginfofname,
            'billinginfolname' => $vendor->billinginfolname,
            'lastccba' => $vendor->lastccba,
            'typeoftransaction' => $vendor->typeoftransaction,
            'datetimestamp' => $vendor->datetimestamp,
            'howdidyoulearn' => $vendor->howdidyoulearn,
            'cname' => $vendor->cname,
            'status' => $vendor->status,
            'note' => $vendor->note

            );
    }
    echo json_encode($data);

    }

    public function markpaidvendorAction($vendorid,$stat) {

        $vendor = Vendorlog::findFirst("id='" . $vendorid ."'");

        if ($vendor) {
            $vendor->status = $stat;
            if($vendor->save()){

                if($vendor->electricity == 'yes'){
                    $sepatotal = $vendor->totalpayment - 56;
                    $elec = '56';
                    $electricity = 'yes';
                }else{
                    $sepatotal = $vendor->totalpayment;
                    $elec = '0';
                    $electricity = 'no';
                }
                $paid = 'Paid';

                $dc = new CB();
                $content = $dc->invoiceVendorTemplate($vendor->contactperson, $vendor->orgname, $vendor->transactionId, $sepatotal, $elec, $vendor->paymentmethod, $vendor->electricity, $vendor->tables, $vendor->totalpayment, date("Y-m-d H:i:s"), $paid);
                $json = json_encode(array(
                    'From' => $dc->config->postmark->signature,
                    'To' => $vendor->email,
                    'Subject' => 'Earth Citizen Organizations Invoice',
                    'HtmlBody' => $content
                    ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                    ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = json_decode(curl_exec($ch2), true);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);

                if ($http_code!=200) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('mailsent' => 'success');
                }

                $data = array('success' => 'Vendor Paid');
            }
        }
        echo json_encode($data);
    }

    public function deletevendorAction($vendorid) {

        $conditions = "id='" . $vendorid . "'";
        $vendor = Vendorlog::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($vendor) {
            if($vendor->delete()){
                $data = array('success' => 'Vendor Deleted');
            }
        }
        echo json_encode($data);
    }


    public static function MailcheckvendorAction(){

        if($_POST){

            $guid = new \Utilities\Guid\Guid();
            $vendorid = $guid->GUID();
            $TransactId = uniqid();

            if($_POST['electricity'] == 'yes'){
                $sepatotal = $_POST['totalpayment'] - 56;
                $elec = '56';
                $electricity = 'yes';
            }else{
                $sepatotal = $_POST['totalpayment'];
                $elec = '0';
                $electricity = 'no';
            }
            $paid = 'Unpaid';

            $vendor = new Vendorlog();

            $vendor->assign(array(
                'id' => $vendorid,
                'transactionId' => $TransactId,
                'datetimestamp' => date("Y-m-d H:i:s"),
                'orgname' => $_POST['orgname'],
                'expooffer' => $_POST['expooffer'],
                'contactperson' => $_POST['contactperson'],
                'phone' => $_POST['phone'],
                'email' => $_POST['email'],
                'address' => $_POST['address'],
                'vendortype' => $_POST['vendortype'],
                'tables' => $_POST['table'],
                'electricity' => $electricity,
                'totalpayment' => $_POST['totalpayment'],
                'paymentmethod' => $_POST['vendortype'],
                'amount' => $_POST['totalpayment'],
                'paymentmode' => 'Mail Check',
                'typeoftransaction' => 'SEATTLE NATURAL HEALING EXPO VENDORS',
                'howdidyoulearn' => $_POST['howdidyoulearn'],
                'cname' => $_POST['cname'],
                'status' => 0,
                'note' => $_POST['note']
                ));

            if($vendor->save()){
                $data = array('success' => 'Thank you for joining us! Please mail your payment check to 10702 NE 68th St, Kirkland, WA 98033 and make the checks payable to ECO');
                $dc = new CB();
                $content = $dc->invoiceVendorTemplate($_POST["contactperson"], $_POST["orgname"], $TransactId, $sepatotal, $elec, $_POST['vendortype'], $electricity, $_POST['table'], $_POST['totalpayment'], date("Y-m-d H:i:s"), $paid);
                $json = json_encode(array(
                    'From' => $dc->config->postmark->signature,
                    'To' => $_POST['email'],
                    'Subject' => 'Earth Citizen Organizations Invoice',
                    'HtmlBody' => $content
                    ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                    ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = json_decode(curl_exec($ch2), true);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);

                if ($http_code!=200) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
            }
        }else{
            $data= array('error' => 'Something went wrong in processing your transaction. Please check the fields and try again.');
        }
        echo json_encode($data);


    }


    public static function CCseattlevendorAction(){

        // var_dump($_POST);
        $dc = new CB();
        $guid = new \Utilities\Guid\Guid();
        $vendorid = $guid->GUID();

        $post_url = $dc->config->authorized->post_url;
        $loginname=$dc->config->authorized->apilogin;
        $transactionkey=$dc->config->authorized->transactionkey;
        $host = $dc->config->authorized->apiurlHost;
        $path = $dc->config->authorized->apiurlVer;


        $donatorfName = $_POST['billingfname'];
        $donatorlName = $_POST['billinglname'];
        $zipcode = $_POST['zipcode'];
        $location = $_POST['al1'];

        $creditTransactId = uniqid();

        $post_values = array(

        // the API Login ID and Transaction Key must be replaced with valid values
        "x_login"           => $loginname,
        "x_tran_key"        => $transactionkey,
        "x_first_name" => $donatorfName,
        "x_last_name" => $donatorlName,

        "x_version"         => "3.1",
        "x_delim_data"      => "TRUE",
        "x_delim_char"      => "|",
        "x_relay_response"  => "FALSE",
        "x_invoice_num"     => $creditTransactId,
        "x_type"            => "AUTH_CAPTURE",
        "x_method"          => "Credit Card",
        "x_card_num"        => $_POST['ccn'],
        "x_exp_date"        => sprintf("%02d", $_POST['expiremonth']).substr( $_POST['expireyear'], -2 ),

        "x_amount"          => $_POST['totalpayment'],
        "x_description"     => "SEATTLE NATURAL HEALING EXPO VENDORS.",

        // Additional fields can be added here as outlined in the AIM integration
        // guide at: http://developer.authorize.net
        );

        $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
        $post_values["x_city"] = $_POST['city'];
        $post_values["x_state"] = $_POST['state'];
        $post_values["x_zip"] = $_POST['zip'];
        $post_values["x_country"] = $_POST['country'];

        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"

        $post_string = "";
        foreach( $post_values as $key => $value )
            { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.

        $line_items = array(
            "item1<|>golf balls<|><|>2<|>18.95<|>Y",
            "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
            "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

        foreach( $line_items as $value )
            { $post_string .= "&x_line_item=" . urlencode( $value ); }


        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
       $response_array = explode($post_values["x_delim_char"],$post_response);

        // The results are output to the screen in the form of an html numbered list.
        if($response_array[0]==1){

            $vendor = new Vendorlog();

            $vendor->assign(array(
                'id' => $vendorid,
                'transactionId' => $creditTransactId,
                'datetimestamp' => date("Y-m-d H:i:s"),
                'orgname' => $_POST['orgname'],
                'expooffer' => $_POST['expooffer'],
                'contactperson' => $_POST['contactperson'],
                'phone' => $_POST['phone'],
                'email' => $_POST['email'],
                'address' => $_POST['address'],
                'vendortype' => $_POST['vendortype'],
                'tables' => $_POST['table'],
                'electricity' => $_POST['electricity'],
                'totalpayment' => $_POST['totalpayment'],
                'paymentmethod' => $_POST['vendortype'],
                'amount' => $_POST['totalpayment'],
                'paymentmode' => 'CreditCard',
                'ccemail' => $_POST['ccemail'],
                'billinginfofname' => $donatorfName,
                'billinginfolname' => $donatorlName,
                'lastccba' => substr($_POST['ccn'], -4),
                'typeoftransaction' => 'SEATTLE NATURAL HEALING EXPO VENDORS',
                'howdidyoulearn' => $_POST['howdidyoulearn'],
                'cname' => $_POST['cname'],
                'status' => 1,
                'note' => $_POST['note']
                ));
            if($vendor->save()){

                $dc = new CB();
                $json = json_encode(array(
                    'From' => $dc->config->postmark->signature,
                    'To' => $_POST['ccemail'],$_POST['email'],
                    'Subject' => 'Earth Citizen Organizations Invoice',
                    'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for joining Seattle Natural Healing Expo Vendors. This letter serves as your transaction receipt. <br> <br> Here’s your payment info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['totalpayment'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                    ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                    ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = json_decode(curl_exec($ch2), true);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);

                if ($http_code!=200) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
            echo json_encode(array('success'=>'Success.', 'code' => $response_array));
            }
        }else{
            echo json_encode(array(
                'error'=>'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                'code' => $response_array
                ));
        }


    }

    public static function EcheckseattlevendorAction(){

        // var_dump($_POST);
        $dc = new CB();
        $guid = new \Utilities\Guid\Guid();
        $vendorid = $guid->GUID();

        $post_url = $dc->config->authorized->post_url;
        $loginname=$dc->config->authorized->apilogin;
        $transactionkey=$dc->config->authorized->transactionkey;
        $host = $dc->config->authorized->apiurlHost;
        $path = $dc->config->authorized->apiurlVer;

        $donatorfName = $_POST['billingfname'];
        $donatorlName = $_POST['billinglname'];
        $zipcode = $_POST['zipcode'];
        $location = $_POST['al1'];

        $creditTransactId = uniqid();
        $post_values = array(

        // the API Login ID and Transaction Key must be replaced with valid values
        "x_login"           => $loginname,
        "x_tran_key"        => $transactionkey,
        "x_first_name" => $donatorfName,
        "x_last_name" => $donatorlName,

        "x_version"         => "3.1",
        "x_delim_data"      => "TRUE",
        "x_delim_char"      => "|",
        "x_relay_response"  => "FALSE",
        "x_invoice_num"     => $creditTransactId,

        "x_method"          => "ECHECK",
        "x_bank_aba_code"   => $_POST['bankrouting'],
        "x_bank_acct_num"   => $_POST['bankaccountnumber'],
        "x_bank_acct_type"  => $_POST['at'],
        "x_bank_name"       => $_POST['bankname'],
        "x_bank_acct_name"  => $_POST['accountname'],


        "x_amount"          => $_POST['totalpayment'],
        "x_description"     => "SEATTLE NATURAL HEALING EXPO VENDORS.",

        // Additional fields can be added here as outlined in the AIM integration
        // guide at: http://developer.authorize.net
        );

        if($_POST['at'] == 'BUSINESSCHECKING'){
            $post_values["x_echeck_type"] = 'CCD';
        }else{
            $post_values["x_echeck_type"] = 'WEB';
        }

        $post_values["x_address"] = $_POST['al1'] . $_POST['al2'];
        $post_values["x_city"] = $_POST['city'];
        $post_values["x_state"] = $_POST['state'];
        $post_values["x_zip"] = $_POST['zip'];
        $post_values["x_country"] = $_POST['country']['name'];

        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
        $post_string = "";
        foreach( $post_values as $key => $value )
            { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.
        /*
        $line_items = array(
            "item1<|>golf balls<|><|>2<|>18.95<|>Y",
            "item2<|>golf bag<|>Wilson golf carry bag, red<|>1<|>39.99<|>Y",
            "item3<|>book<|>Golf for Dummies<|>1<|>21.99<|>Y");

        foreach( $line_items as $value )
            { $post_string .= "&x_line_item=" . urlencode( $value ); }
        */

        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values["x_delim_char"],$post_response);

        // The results are output to the screen in the form of an html numbered list.
        if($response_array[0]==1){

            $vendor = new Vendorlog();

            $vendor->assign(array(
                'id' => $vendorid,
                'transactionId' => $creditTransactId,
                'datetimestamp' => date("Y-m-d H:i:s"),
                'orgname' => $_POST['orgname'],
                'expooffer' => $_POST['expooffer'],
                'contactperson' => $_POST['contactperson'],
                'phone' => $_POST['phone'],
                'email' => $_POST['email'],
                'address' => $_POST['address'],
                'vendortype' => $_POST['vendortype'],
                'tables' => $_POST['table'],
                'electricity' => $_POST['electricity'],
                'totalpayment' => $_POST['totalpayment'],
                'paymentmethod' => $_POST['vendortype'],
                'amount' => $_POST['totalpayment'],
                'paymentmode' => 'eCHECK',
                'ccemail' => $_POST['ccemail'],
                'billinginfofname' => $donatorfName,
                'billinginfolname' => $donatorlName,
                'lastccba' => substr($_POST['ccn'], -4),
                'typeoftransaction' => 'SEATTLE NATURAL HEALING EXPO VENDORS',
                'howdidyoulearn' => $_POST['howdidyoulearn'],
                'cname' => $_POST['cname'],
                'status' => 1,
                'note' => $_POST['note']
                ));
            if($vendor->save()){

                $dc = new CB();
                $json = json_encode(array(
                    'From' => $dc->config->postmark->signature,
                    'To' => $_POST['ccemail'],$_POST['email'],
                    'Subject' => 'Earth Citizen Organizations Invoice',
                    'HtmlBody' => 'Hi, '.$donatorfName.' '.$donatorlName.' <br>  <br> Thank you for joining Seattle Natural Healing Expo Vendors. This letter serves as your transaction receipt. <br> <br> Here’s your payment info: <br> <br>Transaction ID: '.$creditTransactId.' <br>Payment Type: CreditCard <br> Amount: '.$_POST['totalpayment'].'<br> <br>Date: '.date("Y-m-d H:i:s").'<br>'
                    ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$dc->config->postmark->token
                    ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = json_decode(curl_exec($ch2), true);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);

                if ($http_code!=200) {
                    $data = array('error' => $mail->ErrorInfo);
                } else {
                    $data = array('success' => 'success');
                }
            }
            else{
                $data['error'] = "Something went wrong saving the data, please try again.";
                            foreach ($vendor->getMessages() as $message) {
                                echo $message;
                            }
            }
            echo json_encode(array('success'=>'Success.', 'code' => $response_array));
        }else{
            echo json_encode(array(
                'error'=>'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                'code' => $response_array
                ));
        }


    }


}