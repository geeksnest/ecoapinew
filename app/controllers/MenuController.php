<?php

namespace Controllers;

use \Models\Menu as Menu;
use \Models\Menusubs as Menusubs;
use \Models\Menustructure as Menustructure;
use \Models\Pages as Pages;
//use \Models\Pagecategory as Pagecategory;
use \Models\Newscategory as Newscategory;
use \Models\Newstags as Newstags;
use \Models\News as News;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class MenuController extends \Phalcon\Mvc\Controller {


	public function menulistAction($num, $page, $keyword) {
		$app = new CB();
		$offsetfinal = ($page * 10) - 10;
		$sql = "SELECT * FROM menu";
		$sqlCount = 'SELECT COUNT(*) FROM menu';
		if ($keyword != 'null' && $keyword != 'undefined') {
			$sqlconcat = " WHERE menu.name LIKE '%" . $keyword . "%' Or menu.date_created LIKE '%" . $keyword . "%'";
            $sql .= $sqlconcat;
            $sqlCount .= $sqlconcat;
		}
		 if($offsetfinal < 0){
            $offsetfinal = 0;
        }
        $sql .= " LIMIT " . $offsetfinal . ",10";
		$searchresult = $app->dbSelect($sql);
		$totalreportdirty = $app->dbSelect($sqlCount);
		echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty[0]["COUNT(*)"]));
		
	}

	public function submenulistAction($menuID) {
		        $app = new CB();
                 $sql = "SELECT * FROM menu WHERE menuID = '".$menuID."'";
                 $searchresult = $app->dbSelect($sql);
                 $conditions = 'menuID="' . $menuID . '"';
                 $menuedited = Menu::findFirst(array($conditions));

                //  if($menuedited->edited == 0){
                //     $sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menusubs.menuID = '".$menuID."' ORDER BY menustructure.num ASC";
                // }else{
                $sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 0 AND menusubs.menuID = '".$menuID."' ORDER BY menustructure.num ASC";
                    // }
                $searchresult2 = $app->dbSelect($sql2);  
                $sql3 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menusubs.menuID = '".$menuID."' ORDER BY menustructure.display_order ASC";
                $searchresult3 = $app->dbSelect($sql3);
                $sql4 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 2 AND menusubs.menuID = '".$menuID."' ORDER BY menustructure.display_order ASC";
                    $searchresult4 = $app->dbSelect($sql4);
                    $sql5 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 3 AND menusubs.menuID = '".$menuID."' ORDER BY menustructure.display_order ASC";
                    $searchresult5 = $app->dbSelect($sql5);
                    $sql6 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID WHERE menustructure.child = 4 AND menusubs.menuID = '".$menuID."' ORDER BY menustructure.display_order ASC";
                    $searchresult6 = $app->dbSelect($sql6);

                    foreach ($searchresult2 as $key => $value) {
                    	foreach ($searchresult3 as $kk => $value) {
                    		if ($searchresult3[$kk]['parent']==$searchresult2[$key]['submenuID']) {
                    			if ($searchresult3[$kk]['child']=="1")
                    			{
                    				$childid1[] = $searchresult3[$kk];

                    			}

                    		}
                    	}
                    }
            
                    foreach ($searchresult4 as $key4 => $value) {
                    	foreach ($childid1 as $key1 => $value) {
                    		if ($searchresult4[$key4]['parent']==$childid1[$key1]['submenuID']) {
                    			$childid2[] = $searchresult4[$key4];
                    		}
                    	}
                    }
                  
                    foreach ($searchresult5 as $key5 => $value) {
                    	foreach ($childid2 as $key2 => $value) {
                    		if ($searchresult5[$key5]['parent']==$childid2[$key2]['submenuID']) {
                    			$childid3[]=$searchresult5[$key5];
                    		}
                    	}
                    }
                    foreach ($searchresult6 as $key6 => $value) {
                    	foreach ($childid3 as $key3 => $value) {
                    		if ($searchresult6[$key6]['parent']==$childid3[$key3]['submenuID']) {
                    			$childid4[]=$searchresult6[$key6];
                    		}

                    	}
                    }
		echo json_encode(array('data' => $searchresult2, 'menuname' => $searchresult,'child1'=>$childid1,'child2'=>$childid2,'child3'=>$childid3,'child4'=>$childid4));

	}

	public function submenunameAction($submenuID) {
		$app = new CB();
		$sql2 = "SELECT * FROM menusubs WHERE submenuID = '".$submenuID."'";
		$searchresult2 = $app->dbSelect($sql2);
		echo json_encode(array('data' => $searchresult2));

	}

	public function pagelistAction() {
		$app = new CB();
		$sql = "SELECT pages.title,pages.pageslugs FROM pages";
		$sql .= " WHERE pages.status = 1";
		$searchresult = $app->dbSelect($sql);
		echo json_encode(array('data' => $searchresult));

	}

	public function categorylistAction() {
		$app = new CB();
		$sql = "SELECT * FROM newscategory";
		$searchresult = $app->dbSelect($sql);
		//$sql2 = "SELECT * FROM pagecategory";
		//$searchresult2 = $app->dbSelect($sql2),'datapage' => $searchresult2;
		echo json_encode(array('data' => $searchresult));

	}

	public function taglistAction() {
		$app = new CB();
		$sql = "SELECT * FROM newstags";
		$searchresult = $app->dbSelect($sql);
		echo json_encode(array('data' => $searchresult));

	}

	public function postlistAction() {
		$app = new CB();
		$sql = "SELECT * FROM news WHERE status = 1";
		$searchresult = $app->dbSelect($sql);
		echo json_encode(array('data' => $searchresult));

	} 

	/*public function auralistAction() {
		$find = Pages::findFirst("category = 'AURA' AND status = 1");
		if($find) {
			echo json_encode($find);
		}
	}*/

	public function menuAddAction() {

		if($_POST){
			$guid = new \Utilities\Guid\Guid();
			$menuID = $guid->GUID();
			$menu = new Menu();
			$menu->assign(array(
				'menuID' => $menuID,
				'name' => $_POST['newmenu'],
				'shortCode' => '['.$_POST['shortCode'].']',
				'date_created' => date("Y-m-d"),
				'date_updated' => date("Y-m-d H:i:s")
				));

			if (!$menu->save()) {
				$data['error'] = "Something went wrong saving the data, please try again.";
			} else {
				$data['success'] = "Success";
				$app = new CB();
				$sql = "SELECT * FROM menu WHERE menuID = '".$menuID."'";
				$searchresult = $app->dbSelect($sql);

			}
			echo json_encode($searchresult);

		}
	}

	public function savemenuAction($menu,$menuID,$name) {
		$conditions = 'menuID="' . $menuID . '"';
		$deletemenu = Menustructure::find(array($conditions));
		$data = array('error' => 'Not Found');
		if ($deletemenu) {
			if ($deletemenu->delete()) {
				$data = array('success' => 'Menu Deleted');
			}
		}	

		$menudata = json_decode($menu);

		foreach ($menudata as $key)
		{
			$menu = new Menustructure();
			$menu->assign(array(
				'menuID' => $menuID,
				'submenuID' => $key->id,
				'child' => 0
				));

			if ($menu->save()) {		

				$child1 = $key->children;
				if($child1){
					$w = 1;
					foreach ($child1 as $key1){
						$menu1 = new Menustructure();
						$menu1->assign(array(
							'menuID' => $menuID,
							'submenuID' => $key1->id,
							'child' => 1,
							'parent' => $key->id,
							'display_order' => $w
							));

						// $max = Menusubs::find("submenuID='".$key1->id."'");
						// $max->sort = $w;

						if ($menu1->save()) {

							$child2 = $key1->children;
							if($child1){
								$x = 1;
								foreach ($child2 as $key2){
									$menu2 = new Menustructure();
									$menu2->assign(array(
										'menuID' => $menuID,
										'submenuID' => $key2->id,
										'child' => 2,
										'parent' => $key1->id,
										'display_order' => $x
										));

									if ($menu2->save()) {
										$child3 = $key2->children;
										if($child3){
											$y = 1;
											foreach ($child3 as $key3){
												$menu3 = new Menustructure();
												$menu3->assign(array(
													'menuID' => $menuID,
													'submenuID' => $key3->id,
													'child' => 3,
													'parent' => $key2->id,
													'display_order' => $y
													));

												if ($menu3->save()) {
													$child4 = $key3->children;
													if($child4){
														$z = 1;
														foreach ($child4 as $key4){
															$menu4 = new Menustructure();
															$menu4->assign(array(
																'menuID' => $menuID,
																'submenuID' => $key4->id,
																'child' => 4,
																'parent' => $key3->id,
																'display_order' => $z
																));

															if ($menu4->save()) {
																$data['success'] = "Success";
															}
															$z++; 
														}
													}
												}
												$y++; 
											}
										}
									}
									$x++; 
								}
							}
						}						
						$w++; 
					} 
				}

			}
		}


		$menuedited = Menu::findFirst('menuID="' . $menuID . '"');	
		$data2 = array('error' => 'Not Found');
		$menuedited->edited = 1;
		$menuedited->shortCode = '['.$name.']';
		$menuedited->name = $name;
		if ($menuedited->save()) {
			$data2 = array('success' => 'Menu Edited');
		}
		
		echo json_decode($menudata);

	}

	public function submenuAddAction() {

		if($_POST){

			$max = Menusubs::maximum(
				array(
					"column"     => "sort",
					"conditions" => "menuID = '". $_POST['menuID'] ."'"
					)
				);

			$guid = new \Utilities\Guid\Guid();
			$submenuID = $guid->GUID();
			$submenu = new Menusubs();
			$submenu->assign(array(
				'menuID' => $_POST['menuID'],
				'submenuID' => $submenuID,
				'subname' => $_POST['subname'],
				'subtype' => $_POST['path'],
				'sublink' => $_POST['linktext'],
				'newtab' => ($_POST['newtab'] == "" ? "false" : $_POST['newtab']),
				'sort' => $max+1,
				'logo' => $_POST['photo'],
				'date_created' => date("Y-m-d"),
				'date_updated' => date("Y-m-d H:i:s")
				));

			if (!$submenu->save()) {
				$data['error'] = "Something went wrong saving the data, please try again.";
			} else {
				$data['success'] = "Success";
				$menu = Menu::findFirst("menuID = '". $_POST['menuID'] ."'");
				$menu->edited = 1;
				if ($menu->save()) {
					$data['success'] = "Success";
				}
				$menu2 = new Menustructure();
				$menu2->assign(array(
					'menuID' => $_POST['menuID'],
					'submenuID' => $submenuID,
					'child' => 0
					));
				if ($menu2->save()) {
					$data['success'] = "Success";
				}
			}
		}
		echo json_encode($data);
	}


	public function updatesubmenuAddAction() {

		if($_POST){

			$data = array();
			$menu = Menusubs::findFirst('submenuID="' . $_POST['submenuID'] . '"');
			$menu->subname = $_POST['subname'];
			$menu->sublink = $_POST['sublink'];
			$menu->newtab = $_POST['newtab'];
			$menu->subtype = $_POST['subtype'];
			$menu->slugs = $_POST['slugs'];
			if(isset($_POST['photo'])){
			  $menu->logo = $_POST['photo'];
			}
			if (!$menu->save()) {
				$data['error'] = "Something went wrong saving page status, please try again.";
			} else {
				$data['success'] = "Success";
			}

			echo json_encode($_POST);


		}
	}

		public function addsubmenuAction() {
		
			$childmenu = Menustructure::findFirst("submenuID = '".$_POST['parentmenuID']."' ");
			$child = $childmenu->child;
		if($_POST){
			$max = Menusubs::maximum(
				array(
					"column"     => "sort",
					"conditions" => "menuID = '". $_POST['menuID'] ."'"
					)
				);

			$guid = new \Utilities\Guid\Guid();
			$submenuID = $guid->GUID();
			$submenu = new Menusubs();
			$submenu->assign(array(
				'menuID' => $_POST['menuID'],
				'submenuID' => $submenuID,
				'subname' => $_POST['subname'],
				'subtype' => $_POST['path'],
				'sublink' => $_POST['linktext'],
				'newtab' => ($_POST['newtab'] == "" ? "false" : $_POST['newtab']),
				'sort' => $max+1,
				'logo' => $_POST['photo'],
				'date_created' => date("Y-m-d"),
				'date_updated' => date("Y-m-d H:i:s")
				));

			if (!$submenu->save()) {
				$data = "Something went wrong saving the data, please try again.";
			} else {
				$data= "Success";

				$menu2 = new Menustructure();
				$menu2->assign(array(
					'menuID' => $_POST['menuID'],
					'submenuID' => $submenuID,
					'child' => $child+1,
					'parent'=> $_POST['parentmenuID'],
					));
				if ($menu2->save()) {
					$data['success'] = "Success";
				}
				else{
					$data = "error";
				}
			}
		}
		else{
			$data = "wtf";
		}
		echo json_encode($data);
	

	}

	public function menuDeleteAction($menuID) {
		$conditions = 'menuID="' . $menuID . '"';
		$menu = Menu::findFirst(array($conditions));
		$data = array('error' => 'Not Found');
		if ($menu) {
			if ($menu->delete()) {
				$data = array('success' => 'Menu Deleted');
			}
		}
		$submenu = Menusubs::find(array($conditions));
		$data = array('error' => 'Not Found');
		if ($submenu) {
			if ($submenu->delete()) {
				$data = array('success' => 'Menu Deleted');
			}
		}
		$menustruc = Menustructure::find(array($conditions));
		$data = array('error' => 'Not Found');
		if ($menustruc) {
			if ($menustruc->delete()) {
				$data = array('success' => 'Menu Deleted');
			}
		}
		echo json_encode($data);
	}

	public function submenuDeleteAction($submenuID) {
		$conditions = 'submenuID="' . $submenuID . '"';
		$submenu = Menusubs::findFirst(array($conditions));
		$data = array('error' => 'Not Found');
		if ($submenu) {
			if ($submenu->delete()) {
				$data = array('success' => 'Menu Deleted');
			}
		}
		$menustruc = Menustructure::findFirst(array($conditions));
		$data = array('error' => 'Not Found');
		if ($menustruc) {
			if ($menustruc->delete()) {
				$data = array('success' => 'Menu Deleted');
			}
		}
		echo json_encode($data);
	}


	//FRONTEND

	public function viewfrontendmenuAction($shortCode) {

		$shortCode = Menu::findFirst('shortCode="' . $shortCode . '"');
		$menuID = $shortCode->menuID;

		$app = new CB();
		$sql = "SELECT * FROM menu WHERE menuID = '".$menuID."'";
		$searchresult = $app->dbSelect($sql);

		$conditions = 'menuID="' . $menuID . '"';
		$menuedited = Menu::findFirst(array($conditions));
		if($menuedited->edited == 0){
			$sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
			$sql2 .= " WHERE menusubs.menuID = '".$menuID."' ORDER BY menustructure.num ASC";
			$searchresult2 = $app->dbSelect($sql2);	
		}else{
			$sql2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
			$sql2 .= " WHERE menustructure.child = 0 AND menusubs.menuID = '".$menuID."' ORDER BY menustructure.num ASC";
			$searchresult2 = $app->dbSelect($sql2);	

			foreach($searchresult2 as $key => $value){

				$child1 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
				$child1 .= " WHERE menustructure.child = 1 AND parent = '".$value['submenuID']."' ORDER BY display_order ASC";
				$child_1 = $app->dbSelect($child1);	


				foreach($child_1 as $key1 => $value1){

					$child2 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
					$child2 .= " WHERE menustructure.child = 2 AND parent = '".$value1['submenuID']."' ORDER BY display_order ASC";
					$child_2 = $app->dbSelect($child2);


					foreach($child_2 as $key2 => $value2){

						$child3 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
						$child3 .= " WHERE menustructure.child = 3 AND parent = '".$value2['submenuID']."' ORDER BY display_order ASC";
						$child_3 = $app->dbSelect($child3);

						foreach($child_3 as $key3 => $value3){

							$child4 = "SELECT * FROM menusubs LEFT JOIN menustructure ON menusubs.submenuID = menustructure.submenuID";
							$child4 .= " WHERE menustructure.child = 4 AND parent = '".$value3['submenuID']."' ORDER BY display_order ASC";
							$child_4 = $app->dbSelect($child4);						


							$child_3[$key3]['children'] = $child_4;								
						}

						$child_2[$key2]['children'] = $child_3;								
					}

					$child_1[$key1]['children'] = $child_2;								
				}	

				$searchresult2[$key]['children'] = $child_1;	
			}
		}		
		

		echo json_encode(array('data' => $searchresult2, 'menuname' => $searchresult));

	}

}
