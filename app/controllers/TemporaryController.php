<?php

namespace Controllers;

use \Models\Members as Members;
use \Models\Memberstemp as Memberstemp;
use \Models\Centername as Centername;
use \Models\Memberconfirmation as Memberconfirmation;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;
use MailChimp as MailChimp;

class TemporaryController extends \Phalcon\Mvc\Controller
{	
	
	// record who join the pledge
	public function saveTempUserAction(){

		if($_POST){

			$password = sha1($_POST['password']);

			if(isset($_POST['bday']['val'])){
				if($_POST['bday']['val'] < 10){
					$bday = '-0' . $_POST['bday']['val'];
				}else{
					$bday = '-'. $_POST['bday']['val'];
				}				
			}else{$bday = "";}

			if(isset($_POST['bmonth']['val'])){
				if($_POST['bmonth']['val'] < 10){
					$bmonth = '-0' . $_POST['bmonth']['val'];
				}else{
					$bmonth = '-'.$_POST['bmonth']['val'];  
				}
			}else{$bmonth = "";}

			$member = new Memberstemp();
			$member->tempID = $_POST['tempID'];               
			$member->username = $_POST['username'];               
			$member->email = $_POST['email'];
			$member->birthday = $_POST['byear']['val'] .''. $bmonth . '' . $bday;
			$member->gender = $_POST['gender'];
			$member->firstname = $_POST['fname'];
			$member->lastname = $_POST['lname']; 
			$member->location = $_POST['location']['name']; 
			$member->zipcode = $_POST['zipcode'];              
			$member->centername = $_POST['cname'];              
			$member->howdidyoulearn = $_POST['howdidyoulearn'];              
			$member->refer = $_POST['refer'];              
			$member->status = 0;              
			$member->date_created = date("Y-m-d");
			$member->date_updated = date("Y-m-d H:i:s");
			$member->reg_inden = $_POST['reg_inden'];
			if (!$member->save()) {
				$errors = array();
				foreach ($member->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
			} else {
				$data['success'] = "Successfuly added.";
			}
			echo json_encode($data);
		}
		
	}

	// record who join the pledge to registration
	public function paypalregAction($tempID){

		$findTemp = Memberstemp::findFirst("tempID='" . $tempID . "'");
		if($findTemp){			

			$member = new Members();             
			$member->username = $findTemp->username;               
			$member->email = $findTemp->email;
			$member->birthday = $findTemp->birthday;
			$member->gender = $findTemp->gender;
			$member->firstname = $findTemp->firstname;
			$member->lastname = $findTemp->lastname; 
			$member->location = $findTemp->location; 
			$member->zipcode = $findTemp->zipcode;              
			$member->centername = $findTemp->centername;              
			$member->howdidyoulearn = $findTemp->howdidyoulearn;              
			$member->refer = $findTemp->refer;              
			$member->status = 0;              
			$member->date_created = $findTemp->date_created;
			$member->date_updated = $findTemp->date_updated;
			$member->reg_inden = $findTemp->reg_inden;
			if (!$member->save()) {
				$errors = array();
				foreach ($member->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
			} else {
				$data['success'] = "Successfuly added.";

				$confirmation = new Memberconfirmation();
				$confirmationCode = sha1($member->userid);
				$confirmation->assign(array(
					'members_id' => $member->userid,
					'members_code' => $confirmationCode
					));
				if (!$confirmation->save()) {
					$data['error'] = "Something went wrong saving the data, please try again.";
					foreach ($confirmation->getMessages() as $message) {
						echo $message;
					}
				} else {

					$MailChimp = new MailChimp('289ce16d32317e58af6e65afe7a6c538-us10');
					$result = $MailChimp->call('lists/subscribe', array(
						'id'                => 'fc51891e6f',
						'email'             => array('email'=>$findTemp->email),
						'merge_vars'        => array('FNAME'=>$findTemp->firstname, 'LNAME'=>$findTemp->lastname),
						'double_optin'      => false,
						'update_existing'   => true,
						'replace_interests' => false,
						'send_welcome'      => false,
						));


					$dc = new CB();
					$json = json_encode(array(
						'From' => $dc->config->postmark->signature,
						'To' => $findTemp->email,
						'Subject' => 'Earth Citizen Organization Confirmation Email',
						'HtmlBody' => '<h3>Welcome to ECO, '.$findTemp->firstname.' '.$findTemp->lastname.' 
						</h3> Please click the confirmation link below to activate your account. <br/> <br/> 
						<a href="' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $user->userid . '/' . $confirmationCode . '">' . $GLOBALS["baseURL"] .'/'. 'confirmation/' . $member->userid . '/' . $confirmationCode . '</a> 
						<br/><br/> 
						You are the newest earth citizen, and we are very excited that you’re here because it means we are one step further toward 
						mindful living and a sustainable world. By meeting with other like-minded Earth Citizens and joining ECO’s activities, 
						locally and nationally, you will find hope becomes real and change gets easier for you and others. 
						<br/><br/> 
						Please visit our websites to learn more about ECO and our programs. 
						<br/> 
						<a href="www.earthcitizens.org">www.earthcitizens.org </a>
						<br/> 
						<a href="www.heroesconnect.org">www.heroesconnect.org</a>
						<br><br> 
						Thanks, 
						<br><br>
						ECO Staff'
						));

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $dc->config->postmark->url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Accept: application/json',
						'Content-Type: application/json',
						'X-Postmark-Server-Token: '.$dc->config->postmark->token
						));
					curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
					$response = json_decode(curl_exec($ch), true);
					$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);

					if(isset($findTemp->refer)){

						$json = json_encode(array(
							'From' => $dc->config->postmark->signature,
							'To' => $findTemp->refer,
							'Subject' => 'Earth Citizen Organization Member Invitation',
							'HtmlBody' => 'Hello friend, <br/><br/> We are pleased to let you know '.$findTemp->firstname.' '.$findTemp->lastname.' wanted you to become a member of the Earth Citizens Organization (ECO). <br><br> ECO was established as a 501(c)(3) nonprofit organization to promote mindful living for a sustainable world. The name of the organization represents the understanding that we all are citizens of the Earth and share the responsibility for its well-being. <br><br> We all know that we need to change to make our lives on this planet sustainable. To make the change feasible, ECO proposes to start making changes from soft simple things such as the way we breathe, the way we eat, the way we manage our stress, and the way we feel toward other people and other living beings. <br><br> These elements, together with training and practices for mindfulness and leadership, are the foundation of the training and education that ECO provides for the people who desire to initiate the change in their lives and their community. Graduates of our programs return to their communities ready to share what they have learned and experienced. The generous donations of individuals and supporting institutions make it possible for ECO to provide this invaluable education below cost. For a $10 minimum donation, you can become a part of the community of Earth Citizens. <br><br> To join ECO and become an Earth Citizen, please visit the link below. <br> <br> <a href="' . $GLOBALS["baseURL"] . '/donation"> '.$GLOBALS["baseURL"].'/donation </a> <br> <br> Thank you for your kind interest, <br> <br> ECO Staff'
							));

						$ch2 = curl_init();
						curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
						curl_setopt($ch2, CURLOPT_POST, true);
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
							'Accept: application/json',
							'Content-Type: application/json',
							'X-Postmark-Server-Token: '.$dc->config->postmark->token
							));
						curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
						$response = json_decode(curl_exec($ch2), true);
						$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
						curl_close($ch2);
					}

					if ($http_code==200) {
						$data = array('success' => 'success');
					} else {
						$data = array('error' => $response);
					}

				}			
			}			
			echo json_encode($data);
		}
		
	}
}

