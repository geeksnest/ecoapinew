<?php

namespace Controllers;

use \Models\News as News;
use \Models\Projects as Projects;
use \Models\Projectauthor as Projectauthor;
use \Models\Projectsocialmedia as Projectsocialmedia;
use \Models\Projectsdonations as Projectsdonations;
use \Models\Projectsmailaddress as Projectsmailaddress;
use \Models\Notifications as Notifications;
use \Models\Members as Members;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ProjectController extends \Phalcon\Mvc\Controller
{

	public function saveProjectAction()
	{
		if($_POST){

			if($_POST['datedue']){
				$date = $_POST['datedue'];
				$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
				$dates = explode(" ", $date);
				$d = $mont0[$dates[1]].'/'.$dates[2]."/".$dates[3].' '.$_POST['time'];
				$durtype = 'date';
			}else{
				$d = $_POST['days']." days";
				$durtype = 'days';
			}

			$guid = new \Utilities\Guid\Guid();
			$projID = $guid->GUID();

			$proj = new Projects();
			$proj->assign(array(
				'projID' => $projID,
				'projStatus' => 4,
				'projTitle' => $_POST['projTitle'],
				'projSlugs' => $_POST['projSlugs'],
				'projLoc' => $_POST['projLoc'],
				'projDuration' => $d,
				'projDurType' => $durtype,
				'projGoal' => $_POST['projGoal'],
				'projShortDesc' => $_POST['projShortDesc'],
				'projThumb' => $_POST['projThumb'],
				'projAuthorID' => $_POST['projAuthorID'],
				'date_created' => date('Y-m-d'),
				'date_updated' => date("Y-m-d H:i:s")
				));

			if (!$proj->save()) {
				$errors = array();
				foreach ($proj->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
				var_dump($errors);
			} else {
				$data['success'] = "Project saved.";
				echo json_encode($data);
			}
		}
	}

	public function updateProjectAction($projID)
	{
		if($_POST){

			if($_POST['datedue']){
				$date = $_POST['datedue'];
				if($_POST['datechange'] != 'true'){
					$dates = explode("-", $date);
					$d = $dates[1].'/'.$dates[2]."/".$dates[0].' '.$_POST['time'];
				}else{
					$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
					$dates = explode(" ", $date);
					$d = $mont0[$dates[1]].'/'.$dates[2]."/".$dates[3].' '.$_POST['time'];
				}
				$durtype = 'date';
			}else{
				$d = $_POST['days']." days";
				$durtype = 'days';
			}

			$proj = Projects::findFirst("projID='" . $projID ."'");
			if($proj){
				$proj->projTitle = $_POST['projTitle'];
				$proj->projSlugs = $_POST['projSlugs'];
				$proj->projLoc = $_POST['projLoc'];
				$proj->projDuration = $d;
				$proj->projDurType = $durtype;
				$proj->projGoal = $_POST['projGoal'];
				$proj->projShortDesc = $_POST['projShortDesc'];
				$proj->projDesc = $_POST['projDesc'];
				if($_POST['projWeb']!='null'){
							$proj->projWeb = $_POST['projWeb'];
				}
				$proj->projThumb = $_POST['projThumb'];

				if (!$proj->save()) {
					$errors = array();
					foreach ($proj->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					echo json_encode(array('error' => $errors));
				} else {
					$data['success'] = "Project Updated";

					$projC = Projectsmailaddress::findFirst("projID='" . $projID ."'");
					if($projC){
						$projC->projCName = $_POST['projCName'];
						$projC->projCAddress1 = $_POST['projCAddress1'];
						$projC->projCAddress2 = $_POST['projCAddress2'];
						$projC->projCCity = $_POST['projCCity'];
						$projC->projCState = $_POST['projCState'];
						$projC->projCZip = $_POST['projCZip'];
						$projC->projCCountry = $_POST['projCCountry'];

						if (!$projC->save()) {
							$errors = array();
							foreach ($projC->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success_projC'] = "Mailing Address Updated.";
						}

					}else{
						$mailingAddress = new Projectsmailaddress();
						$mailingAddress->projID = $projID;
						$mailingAddress->projCName = $_POST['projCName'];
						$mailingAddress->projCAddress1 = $_POST['projCAddress1'];
						$mailingAddress->projCAddress2 = $_POST['projCAddress2'];
						$mailingAddress->projCCity = $_POST['projCCity'];
						$mailingAddress->projCState = $_POST['projCState'];
						$mailingAddress->projCZip = $_POST['projCZip'];
						$mailingAddress->projCCountry = $_POST['projCCountry'];

						if (!$mailingAddress->save()) {
							$errors = array();
							foreach ($mailingAddress->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success_mailingAddress'] = "Mailing Address Added.";
						}
					}

					$projM = Projectsocialmedia::findFirst("projID='" . $projID ."'");
					if($projM){
						if($_POST['facebook']!='null'){
							$projM->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$projM->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$projM->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$projM->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$projM->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$projM->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$projM->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$projM->instagram = $_POST['instagram'];
						}
						if (!$projM->save()) {
							$errors = array();
							foreach ($projM->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success_projM'] = "Social Updated.";
						}

					}else{
						$socialAdd = new Projectsocialmedia();
						$socialAdd->projID = $projID;
						if($_POST['facebook']!='null'){
							$socialAdd->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$socialAdd->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$socialAdd->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$socialAdd->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$socialAdd->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$socialAdd->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$socialAdd->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$socialAdd->instagram = $_POST['instagram'];
						}

						if (!$socialAdd->save()){
							$errors = array();
							foreach ($socialAdd->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error3' => $errors));
						}
						else {
							$data['success_socialAdd'] = "Social Updated.";
						}
					}

				}
			}
		}
		echo json_encode($data);
	}

	public function updateSocialAction()
	{
		if($_POST){
			$proj = Projects::findFirst("projID='" . $_POST['projID']."'");
			if($proj){
				if($_POST['projWeb']!='null'){
							$proj->projWeb = $_POST['projWeb'];
				}

				if (!$proj->save()) {
					$errors = array();
					foreach ($proj->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					echo json_encode(array('error' => $errors));
				} else {
					$data['success'] = "Website Saved.";

					$projM = Projectsocialmedia::findFirst("projID='" . $_POST['projID'] ."'");
					if($projM){
						if($_POST['facebook']!='null'){
							$projM->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$projM->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$projM->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$projM->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$projM->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$projM->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$projM->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$projM->instagram = $_POST['instagram'];
						}
						if (!$projM->save()) {
							$errors = array();
							foreach ($projM->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success2'] = "Social Updated.";
						}

					}else{
						$socialAdd = new Projectsocialmedia();
						$socialAdd->projID = $_POST['projID'];
						if($_POST['facebook']!='null'){
							$socialAdd->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$socialAdd->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$socialAdd->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$socialAdd->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$socialAdd->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$socialAdd->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$socialAdd->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$socialAdd->instagram = $_POST['instagram'];
						}

						if (!$socialAdd->save()){
							$errors = array();
							foreach ($socialAdd->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error3' => $errors));
						}
						else {
							$data['success3'] = "Social Saved.";
						}
					}
				}

			}
		}

		echo json_encode($data);
	}

	public function felistAction()
	{

		$app = new CB();

		$conditions = "SELECT * FROM projects ";
		$conditions .= "LEFT JOIN members ON projects.projAuthorID = members.userid ";
		$conditions .= "WHERE projects.projStatus = 2 ";
		$conditions .= "ORDER BY projects.num DESC";
		$searchresult = $app->dbSelect($conditions);
		foreach($searchresult as $sr=>$val){
			$projTotalDonations = Projectsdonations::sum(array("column" => "amount", "donatedto = '".$val['projID']."'"));
			$x = ($projTotalDonations/$val['projGoal']);
			$projTotalPercentage = $x * 100;
			if($projTotalDonations == null){
				$donations = 0;
			}else{
				$donations = $projTotalDonations;
			}

			$percentage = explode(".", $projTotalPercentage);
			$searchresult[$sr]['projTotalDonations'] = (int) $donations;
			$searchresult[$sr]['projTotalPercentage'] = (int) $percentage[0];
		}
		echo json_encode(array('data' => $searchresult));
	}

	public function felistmoreAction($offset, $num)
	{

		$app = new CB();

		$conditions = "SELECT * FROM projects ";
		$conditions .= "LEFT JOIN members ON projects.projAuthorID = members.userid ";
		$conditions .= "WHERE projects.projStatus = 2 ";
		$conditions .= "ORDER BY projects.num DESC";  
		$limit = " LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);
		foreach($searchresult as $sr=>$val){
			$projTotalDonations = Projectsdonations::sum(array("column" => "amount", "donatedto = '".$val['projID']."'"));
			$x = ($projTotalDonations/$val['projGoal']);
			$projTotalPercentage = $x * 100;
			if($projTotalDonations == null){
				$donations = 0;
			}else{
				$donations = $projTotalDonations;
			}

			$percentage = explode(".", $projTotalPercentage);
			$searchresult[$sr]['projTotalDonations'] = (int) $donations;
			$searchresult[$sr]['projTotalPercentage'] = (int) $percentage[0];
		}
		echo json_encode(array('data' => $searchresult, 'count' => count($count)));
	}

	public function felistmorefAction($offset, $num)
	{

		$app = new CB();

		$conditions = "SELECT * FROM projects ";
		$conditions .= "LEFT JOIN members ON projects.projAuthorID = members.userid ";
		$conditions .= "WHERE projects.projStatus = 5 ";
		$conditions .= "ORDER BY projects.num DESC";  
		$limit = " LIMIT ".$offset.", ". $num .";                                                                                                                                                                        ";
		$searchresult = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);
		foreach($searchresult as $sr=>$val){
			$projTotalDonations = Projectsdonations::sum(array("column" => "amount", "donatedto = '".$val['projID']."'"));
			$x = ($projTotalDonations/$val['projGoal']);
			$projTotalPercentage = $x * 100;
			if($projTotalDonations == null){
				$donations = 0;
			}else{
				$donations = $projTotalDonations;
			}

			$percentage = explode(".", $projTotalPercentage);
			$searchresult[$sr]['projTotalDonations'] = (int) $donations;
			$searchresult[$sr]['projTotalPercentage'] = (int) $percentage[0];
		}
		echo json_encode(array('data' => $searchresult, 'count' => count($count)));
	}

	public function viewAction($slugs)
	{
		$app = new CB();
		$conditions = "SELECT * FROM projects ";
		$conditions .= "LEFT JOIN members ON projects.projAuthorID = members.userid ";
		$conditions .= "LEFT JOIN projectsocialmedia ON projects.projID = projectsocialmedia.projID ";
		$conditions .= "LEFT JOIN projectsmailaddress ON projects.projID = projectsmailaddress.projID ";
		$conditions .= "WHERE projects.projSlugs LIKE '".$slugs."'";
		$searchresult = $app->dbSelect($conditions);
		$proj = Projects::findFirst("projSlugs='" . $slugs ."'");
		$socialsquery = "SELECT * FROM projectsocialmedia WHERE projID LIKE '".$proj->projID."'";
		$socials = $app->dbSelect($socialsquery);
		$donquery = "SELECT sum(amount) as total, count(amount) as donors FROM projectsdonations WHERE donatedto LIKE '".$proj->projID."'";
		$donations = $app->dbSelect($donquery);
		echo json_encode(array(
			'data' => $searchresult,
			'donations' => $donations,
			'projID' => $proj->projID,
			'daystogo' => strtotime('11 days')
			));
	}

	public function viewMyProjectAction($slugs)
	{
		$app = new CB();
		$conditions = "SELECT * FROM projects ";
		$conditions .= "LEFT JOIN members ON projects.projAuthorID = members.userid ";
		$conditions .= "LEFT JOIN projectsocialmedia ON projects.projID = projectsocialmedia.projID ";
		$conditions .= "LEFT JOIN projectsmailaddress ON projects.projID = projectsmailaddress.projID ";
		$conditions .= "WHERE projects.projSlugs LIKE '".$slugs."'";
		$searchresult = $app->dbSelect($conditions);
		$proj = Projects::findFirst("projSlugs='" . $slugs ."'");
		$socialsquery = "SELECT * FROM projectsocialmedia WHERE projID LIKE '".$proj->projID."'";
		$socials = $app->dbSelect($socialsquery);
		$donquery = "SELECT * FROM projectsdonations WHERE donatedto LIKE '".$proj->projID."'";
		$donations = $app->dbSelect($donquery);
		$donCountsquery = "SELECT sum(amount) as total, count(amount) as donors FROM projectsdonations WHERE donatedto LIKE '".$proj->projID."'";
		$donCounts = $app->dbSelect($donCountsquery);
		echo json_encode(array(
			'data' => $searchresult,
			'socials' => $socials, 
			'donations' => $donations,
			'donCounts' => $donCounts, 
			'projID' => $proj->projID));
	}

	public function myprojAction($userid,$page)
	{
		$app = new CB();
		$offsetfinal = ($page * 10) - 10;

		$conditions = "SELECT projID,projTitle,projSlugs,projAuthorID,projStatus,date_created,countStart FROM projects ";
		$conditions .= "WHERE projAuthorID LIKE '".$userid."'";
		$searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

		$count = $app->dbSelect($conditions);

		echo json_encode(array(
			'data' => $searchresult,
			'index' => $page,
			'total_items' => count($count)
			));
	}

	public function deleteMyProjAction($projID)
	{
		$proj = Projects::findFirst("projID='" . $projID ."'");
		if ($proj) {
        if ($proj->delete()) {
            $data = array('success' => 'Project Deleted.');
        }
    }
    echo json_encode($data);
	}


	public function submitprojectAction($projID)
	{
		if($_POST){

			if($_POST['datedue']){
				$date = $_POST['datedue'];
				if($_POST['datechange'] != 'true'){
					$dates = explode("-", $date);
					$d = $dates[1].'/'.$dates[2]."/".$dates[0].' '.$_POST['time'];
				}else{
					$mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
					$dates = explode(" ", $date);
					$d = $mont0[$dates[1]].'/'.$dates[2]."/".$dates[3].' '.$_POST['time'];
				}
				$durtype = 'date';
			}else{
				$d = $_POST['days']." days";
				$durtype = 'days';
			}

			$proj = Projects::findFirst("projID='" . $projID ."'");
			if($proj){
				$proj->projTitle = $_POST['projTitle'];
				$proj->projSlugs = $_POST['projSlugs'];
				$proj->projLoc = $_POST['projLoc'];
				$proj->projDuration = $d;
				$proj->projDurType = $durtype;
				$proj->projGoal = $_POST['projGoal'];
				$proj->projShortDesc = $_POST['projShortDesc'];
				$proj->projDesc = $_POST['projDesc'];
				if($_POST['projWeb']!='null'){
							$proj->projWeb = $_POST['projWeb'];
				}
				$proj->projStatus = 0;
				// $proj->date_approved = date("Y-m-d H:i:s");
				
				$dateInput = explode('-',$proj->date_created);
				$dayInput = explode(' ', $dateInput[2]);
				$datecreated = $dateInput[1].'-'.$dayInput[0].'-'.$dateInput[0];
				if (!$proj->save()) {
					$errors = array();
					foreach ($proj->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					echo json_encode(array('error' => $errors));
				} else {
					$data['success'] = $datecreated;
					$member = Members::findFirst("userid='" . $proj->projAuthorID ."'");
					$dc = new CB();
					$content = $dc->submitProjEmail($_POST['projTitle'],$member->firstname,$member->lastname,$_POST['projShortDesc'],$d,$_POST['projGoal'],$datecreated);
					$json = json_encode(array(
						'From' => $dc->config->postmark->signature,
						'To' => 'info@earthcitizens.org',
						'Subject' => "ECO New Project",
						'HtmlBody' => $content
						));

					$ch2 = curl_init();
					curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
					curl_setopt($ch2, CURLOPT_POST, true);
					curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
						'Accept: application/json',
						'Content-Type: application/json',
						'X-Postmark-Server-Token: '.$dc->config->postmark->token
						));
					curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
					$response = json_decode(curl_exec($ch2), true);
					$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
					curl_close($ch2);

					if ($http_code!=200) {
						$data['success3'] = $mail->ErrorInfo;
					} else {
						$data['success3'] = "emailsent";
					}

					$projC = Projectsmailaddress::findFirst("projID='" . $projID ."'");
					if($projC){
						$projC->projCName = $_POST['projCName'];
						$projC->projCAddress1 = $_POST['projCAddress1'];
						$projC->projCAddress2 = $_POST['projCAddress2'];
						$projC->projCCity = $_POST['projCCity'];
						$projC->projCState = $_POST['projCState'];
						$projC->projCZip = $_POST['projCZip'];
						$projC->projCCountry = $_POST['projCCountry'];

						if (!$projC->save()) {
							$errors = array();
							foreach ($projC->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success_projC'] = "Mailing Address Updated.";
						}

					}else{
						$mailingAddress = new Projectsmailaddress();
						$mailingAddress->projID = $projID;
						$mailingAddress->projCName = $_POST['projCName'];
						$mailingAddress->projCAddress1 = $_POST['projCAddress1'];
						$mailingAddress->projCAddress2 = $_POST['projCAddress2'];
						$mailingAddress->projCCity = $_POST['projCCity'];
						$mailingAddress->projCState = $_POST['projCState'];
						$mailingAddress->projCZip = $_POST['projCZip'];
						$mailingAddress->projCCountry = $_POST['projCCountry'];

						if (!$mailingAddress->save()) {
							$errors = array();
							foreach ($mailingAddress->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success_mailingAddress'] = "Mailing Address Added.";
						}
					}

					$projM = Projectsocialmedia::findFirst("projID='" . $projID ."'");
					if($projM){
						if($_POST['facebook']!='null'){
							$projM->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$projM->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$projM->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$projM->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$projM->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$projM->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$projM->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$projM->instagram = $_POST['instagram'];
						}
						if (!$projM->save()) {
							$errors = array();
							foreach ($projM->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success2'] = "Social Updated.";
						}

					}else{
						$socialAdd = new Projectsocialmedia();
						$socialAdd->projID = $projID;
						if($_POST['facebook']!='null'){
							$socialAdd->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$socialAdd->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$socialAdd->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$socialAdd->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$socialAdd->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$socialAdd->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$socialAdd->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$socialAdd->instagram = $_POST['instagram'];
						}

						if (!$socialAdd->save()){
							$errors = array();
							foreach ($socialAdd->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error3' => $errors));
						}
						else {
							$data['success3'] = "Social Updated.";
						}
					}

				}
			}
		}
		echo json_encode($data);
	}


	public function publishAction($projID)
	{
		if($_POST){

			$proj = Projects::findFirst("projID='" . $projID ."'");
			if($proj){
				date_default_timezone_set('UTC');
				$daystogo = strtotime($proj->projDuration); //convert days(ex. 30 days) or dates(ex. 01/20/2016 15:30:00) to Epoch Time
				if($_POST['projWeb']!='null'){
							$proj->projWeb = $_POST['projWeb'];
				}
				$proj->projStatus = 2;
				$proj->date_published = date("Y-m-d H:i:s");
				$proj->countStart = $daystogo * 1000; //add 3 zeros to Epoch Time = milliseconds

				if (!$proj->save()) {
					$errors = array();
					foreach ($proj->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					echo json_encode(array('error' => $errors));
				} else {
					$data['success'] = "Project Published.";

					$projM = Projectsocialmedia::findFirst("projID='" . $projID ."'");
					if($projM){
						if($_POST['facebook']!='null'){
							$projM->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$projM->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$projM->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$projM->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$projM->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$projM->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$projM->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$projM->instagram = $_POST['instagram'];
						}
						if (!$projM->save()) {
							$errors = array();
							foreach ($projM->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error2' => $errors));
						} else {
							$data['success2'] = "Social Updated.";
						}

					}else{
						$socialAdd = new Projectsocialmedia();

						if($_POST['facebook']!='null'){
							$socialAdd->facebook = $_POST['facebook'];
						}
						if($_POST['youtube']!='null'){
							$socialAdd->youtube = $_POST['youtube'];
						}
						if($_POST['twitter']!='null'){
							$socialAdd->twitter = $_POST['twitter'];
						}
						if($_POST['linkedin']!='null'){
							$socialAdd->linkedin = $_POST['linkedin'];
						}
						if($_POST['pinterest']!='null'){
							$socialAdd->pinterest = $_POST['pinterest'];
						}
						if($_POST['google']!='null'){
							$socialAdd->google = $_POST['google'];
						}
						if($_POST['tumblr']!='null'){
							$socialAdd->tumblr = $_POST['tumblr'];
						}
						if($_POST['instagram']!='null'){
							$socialAdd->instagram = $_POST['instagram'];
						}

						if (!$socialAdd->save()){
							$errors = array();
							foreach ($socialAdd->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							echo json_encode(array('error3' => $errors));
						}
						else {
							$data['success3'] = "Social Updated.";
						}
					}

				}
			}
		}
		echo json_encode($data);
	}


	public function checkExpiredAction()
	{
		$app = new CB();

		$conditions = "SELECT * FROM projects ";
		$conditions .= "WHERE projStatus = 2";
		// $conditions .= "ORDER BY projects.num DESC LIMIT ".$offset.", ". $page.";                                                                                                                                                                         ";
		$searchresult = $app->dbSelect($conditions);
		foreach($searchresult as $sr=>$val){
			$projTotalDonations = Projectsdonations::sum(array("column" => "amount", "donatedto = '".$val['projID']."'"));

			$daystogo = strtotime(date("Y-m-d H:i:s")).'000';
			if($daystogo >= $val['countStart']){
				$proj = Projects::findFirst("projID='" . $val['projID'] ."'");
				if($proj){
					$proj->projStatus = 5;
					if (!$proj->save()){
						$errors = array();
						foreach ($proj->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						$data['error'] = $errors;
					}
					else {
						$data['success'] = "Social Updated.";
						$guid = new \Utilities\Guid\Guid();
						$noteID = $guid->GUID();
						$note = new Notifications();
						$note->assign(array(
							'noteID' => $noteID,
							'userID' => $proj->projAuthorID,
							'readStatus' => 0,
							'content' => $proj->projTitle,
							'noteType' => 6,
							'itemID' =>  $proj->projID,
							'timeStarted' => time().'000',
							'date_created' => date("Y-m-d H:i:s")
							));

						if (!$note->save()) {
							$errors = array();
							foreach ($note->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							die(json_encode(array('error' => $errors)));
						} else {
							$data['notesuccess'] = "Notification saved.";
						}
					}
				}else{
					$data['ngek'] = "wala naman.";
				}

				$member = Members::findFirst("userid='" . $proj->projAuthorID ."'");
				$dc = new CB();
				$content = $dc->finishedProjEmail($proj->projTitle,$member->firstname,$member->lastname,$dc->config->application->baseURL,$projTotalDonations);
				$json = json_encode(array(
					'From' => $dc->config->postmark->signature,
					'To' => $member->email,
					'Subject' => "ECO Projects",
					'HtmlBody' => $content
					));

				$ch2 = curl_init();
				curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
				curl_setopt($ch2, CURLOPT_POST, true);
				curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
					'Accept: application/json',
					'Content-Type: application/json',
					'X-Postmark-Server-Token: '.$dc->config->postmark->token
					));
				curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
				$response = json_decode(curl_exec($ch2), true);
				$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
				curl_close($ch2);

				if ($http_code!=200) {
					$data = array('error' => $mail->ErrorInfo);
				} else {
					$data = array('success' => 'success');
				}
				
			}

		}
		echo json_encode($data);
	}

	// Backend part

	public function belistAction($num, $page, $keyword, $sort, $sortto) {
		$app = new CB();
		$offsetfinal = ($page * 10) - 10;
			// switch (strtolower($keyword)) {
			// 	case 'finished':
			// 		$keyword = 5;
			// 		break;
			// 		case 'disapproved':
			// 		$keyword = 3;
			// 		break;
			// 	default:
			// 		$keyword = $keyword;
			// 		break;
			// }
		if ($keyword == 'null' || $keyword == 'undefined') {
			$conditions = "SELECT * FROM projects";
			$conditions .= " LEFT JOIN members ON projects.projAuthorID = members.userid";
			$conditions .= " WHERE projStatus = 0 OR projStatus = 1 OR projStatus = 2 OR projStatus = 3 OR projStatus = 5 ";
			
		} else {
			$conditions = "SELECT * FROM projects";
			$conditions .= " LEFT JOIN members ON projects.projAuthorID = members.userid";
			$conditions .= " WHERE projTitle LIKE '%" . $keyword . "%' OR projLoc LIKE '%" . $keyword . "%'";
			$conditions .= " OR projDuration LIKE '" . $keyword . "' OR firstname LIKE '%" . $keyword . "%' OR lastname LIKE '%" . $keyword . "%'
			 OR projStatus LIKE '" . $keyword . "' OR projStatus LIKE '" . $keyword . "' OR date_created LIKE '%" . $keyword . "%'
			 OR date_approved LIKE '%" . $keyword . "%' ";
			// $conditions .= " OR projStatus = 0 OR projStatus = 1 OR projStatus = 2 OR projStatus = 3 OR projStatus = 5 ";
			
			// echo json_encode($d);
		}

		if($sortto == 'DESC'){
			$sortby = "ORDER BY $sort DESC";
		}else{
			$sortby = "ORDER BY $sort ASC";
		}

		$conditions .= $sortby;

		$searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

		$count = $app->dbSelect($conditions);

		echo json_encode(array(
			'data' => $searchresult,
			'index' => $page,
			'total_items' => count($count)
			));
	}

	public function projectdonationsAction($num, $page, $keyword, $projID, $sort, $sortto) {
		$app = new CB();
		$offsetfinal = ($page * 10) - 10;

		if ($keyword == 'null' || $keyword == 'undefined') {
			$conditions = "SELECT * FROM projectsdonations WHERE donatedto LIKE '" . $projID . "' ";
		} else {
			$conditions = "SELECT * FROM projectsdonations";
			$conditions .= " WHERE transactionId LIKE '%" . $keyword . "%' AND donatedto LIKE '" . $projID . "' OR billinginfofname LIKE '%" . $keyword . "%' AND donatedto LIKE '" . $projID . "'";
			$conditions .= " OR billinginfolname LIKE '" . $keyword . "' AND donatedto LIKE '" . $projID . "' OR paymentmode LIKE '" . $keyword . "' AND donatedto LIKE '" . $projID . "'";
			$conditions .= " OR amount LIKE '" . $keyword . "' AND donatedto LIKE '" . $projID . "' OR datetimestamp LIKE '" . $keyword . "' OR useremail LIKE '" . $keyword . "'";
			$conditions .= " AND donatedto LIKE '" . $projID . "' ";
		}

		if($sortto == 'DESC'){
			$sortby = "ORDER BY $sort DESC";
		}else{
			$sortby = "ORDER BY $sort ASC";
		}

		$conditions .= $sortby;

		$searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

		$count = $app->dbSelect($conditions);

		echo json_encode(array(
			'data' => $searchresult,
			'index' => $page,
			'total_items' => count($count)
			));
	}


	public function deleteDonationAction($id)
	{
		$don = Projectsdonations::findFirst("num=" . $id ."");
		if ($don) {
        if ($don->delete()) {
            $data = array('success' => 'Donation Deleted.');
        }
    }
    echo json_encode($data);
	}

	public function reviewAction($projID)
	{

		$app = new CB();
		$conditions = "SELECT * FROM projects ";
		$conditions .= "LEFT JOIN members ON projects.projAuthorID = members.userid ";
		$conditions .= "LEFT JOIN projectsocialmedia ON projects.projID = projectsocialmedia.projID ";
		$conditions .= "LEFT JOIN projectsmailaddress ON projects.projID = projectsmailaddress.projID ";
		$conditions .= "WHERE projects.projID LIKE '".$projID."'";
		$searchresult = $app->dbSelect($conditions);
		$proj = Projects::findFirst("projID='" . $projID ."'");
		$socialsquery = "SELECT * FROM projectsocialmedia WHERE projID LIKE '".$proj->projID."'";
		$socials = $app->dbSelect($socialsquery);
		$donquery = "SELECT sum(amount) as total, count(amount) as donors FROM projectsdonations WHERE donatedto LIKE '".$proj->projID."'";
		$donations = $app->dbSelect($donquery);
		echo json_encode(array(
			'data' => $searchresult,
			'donations' => $donations,
			'projID' => $proj->projID,
			'daystogo' => strtotime('11 days')
			));
	}

	public function resolutionAction($projID,$status)
	{
		$proj = Projects::findFirst("projID='" . $projID ."'");
		if($proj){
			if($status == 3){
				$proj->msg =  $_POST['msg'];
			}
			$proj->projStatus = $status;
			$proj->date_approved = date("Y-m-d H:i:s");
			if (!$proj->save()) {
				$errors = array();
				foreach ($proj->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				die(json_encode(array('error' => $errors)));
			} else {
				$data['success'] = "Status Updated.";
				$guid = new \Utilities\Guid\Guid();
				$noteID = $guid->GUID();
				$note = new Notifications();
				$note->assign(array(
					'noteID' => $noteID,
					'userID' => $proj->projAuthorID,
					'readStatus' => 0,
					'content' => $proj->projTitle,
					'noteType' => $status,
					'itemID' =>  $proj->projID,
					'timeStarted' => time().'000',
					'date_created' => date("Y-m-d H:i:s")
					));

				if (!$note->save()) {
					$errors = array();
					foreach ($note->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					die(json_encode(array('error' => $errors)));
				} else {
					$data['notesuccess'] = "Notification saved.";
				}
				$member = Members::findFirst("userid='" . $proj->projAuthorID ."'");

				$dc = new CB();
				$content = $dc->approvedProjEmail($proj->projTitle,$member->firstname,$member->lastname,$dc->config->application->baseURL,$status,$_POST['msg']);
				$json = json_encode(array(
					'From' => $dc->config->postmark->signature,
					'To' => $member->email,
					'Subject' => "ECO Projects",
					'HtmlBody' => $content
					));

				$ch2 = curl_init();
				curl_setopt($ch2, CURLOPT_URL, $dc->config->postmark->url);
				curl_setopt($ch2, CURLOPT_POST, true);
				curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
					'Accept: application/json',
					'Content-Type: application/json',
					'X-Postmark-Server-Token: '.$dc->config->postmark->token
					));
				curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
				$response = json_decode(curl_exec($ch2), true);
				$http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
				curl_close($ch2);

				if ($http_code!=200) {
					$data = array('error' => $mail->ErrorInfo);
				} else {
					$data = array('success' => 'success');
				}
			}
		}
		echo json_encode($data);
	}
}

