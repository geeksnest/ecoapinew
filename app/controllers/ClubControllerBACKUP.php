<?php

namespace Controllers;

use \Models\Club as Club;
use \Models\Members as Members;
use \Controllers\ControllerBase as CB;
use PHPMailer as PHPMailer;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ClubController extends \Phalcon\Mvc\Controller
{

  public function addAction()
  {
    if($_POST){

      $guid = new \Utilities\Guid\Guid();
      $ID = $guid->GUID();

      // $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
   //          $dates = explode(" ", $_POST['datecreated']);
   //          $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

      $club = new Club();
      $club->id = $ID;
      $club->status = "Submitted";
      $club->name = $_POST['name'];
      $club->description = $_POST['description'];
      $club->address1 = $_POST['address1'];
      $club->address2 = $_POST['address2'];
      $club->city = $_POST['city'];
      $club->state = $_POST['state'];
      $club->postal = $_POST['postal'];
      $club->country = $_POST['country'];
      // $club->characteristics = $_POST['characteristics'];
      $club->age = $_POST['age'];
      $club->interests = $_POST['interests'];
      $club->datecreated = $_POST['datecreated'];
      $club->meeting = $_POST['meeting'];
      $club->website = $_POST['website'];
      $club->facebook = $_POST['facebook'];
      $club->contactperson = $_POST['contactperson'];
      $club->contactnumber = $_POST['contactnumber'];
      $club->emailadd = $_POST['emailadd'];
      $club->image = $_POST['image'];
      $club->date_created = date("Y-m-d H:i:s");
      $club->date_updated = date("Y-m-d H:i:s");
      $club->createdby = $_POST['createdby'];
      $member = Members::findFirst("userid=".$_POST['createdby']."");

      $dc = new CB();

      if(!$club->save()){
        $errors = array();
        foreach ($atw->getMessages() as $message){
          $errors[] = $message->getMessage();
        }
        echo json_encode(array('error' => $errors));
      }else{
      $content ='
                 <!doctype html>
                <html class=no-js lang=en data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
                <head>
                    <meta charset="utf-8" />
                    <meta name=viewport content="width=device-width, initial-scale=1.0" />
                    <title>Earth Citizens | ECO CLUB</title>
                    <meta name=description content="Documentation and reference library for ZURB Foundation. JavaScript, CSS, components, grid and more." />
                    <meta name=author content="ZURB, inc. ZURB network also includes zurb.com" />
                    <meta name=copyright content="ZURB, inc. Copyright (c) 2015" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.css" />
                    <link rel=stylesheet type=text/css href="https://earthcitizens.org/css/fr/font-awesome.min.css" />
                </head>
                <body style="background: #fff;color: #222;padding: 0;margin: 0;font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif; font-weight: normal;font-style: normal;line-height: 1.5;position: relative;cursor: auto; font-size: 100%;">
                  <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
                    <div style="width: 100%;">
                      <div style="padding-top: 25px;padding-bottom: 15px;overflow: hidden;">
                        <div style="width: 100%;">
                          <div style="width: 200px;margin-left: auto;margin-right: auto;margin-bottom: 25px;">
                            <img src="https://earthcitizens.org/images/template_images/ecologo1.png" style="width:100%;">
                          </div>
                        </div>
                        <div style="width:100%;float:right">
                          <h2 style="font:20px/25px Helvetica,Arial,sans-serif;color:#717171;margin-bottom: 0px;">ECO CLUB</h2>
                          <p style="text-align: justify;color: #555; margin-top: 0px;font:Arial,sans-serif;">
                           
                          </p>
                        </div>
                      </div>
                      <hr style="border 2px solid #000" />
                      <div style=width:25%;float:left;height:35px;padding-top:15px;color:#555;font:Arial,sans-serif>Request Date: <b>'.date("Y-m-d").'</b></div>
                      <div style="width: 75%;float:right;">
                        <div style="overflow: hidden;width: 300px;height:50px;float:right;">
                          <ul style="margin-left: auto;margin-right: auto;padding: 0; list-style: none;">
                            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA;font:Arial,sans-serif;" href="https://www.facebook.com/EarthCitizensOrganization">LIKE <img style="width:20px;height:20px" src="https://earthcitizens.org/images/facebook.png"></a></li>
                            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA;font:Arial,sans-serif;" href="https://twitter.com/goearthcitizens">FOLLOW <img style="width:20px;height:20px" src="https://earthcitizens.org/images/twitter.png"></a></li>
                            <li style="list-style: none;float: left;margin-left: 1.375rem;display: block;"><a style="text-decoration:none;color:#008CBA;font:Arial,sans-serif;" href="#">REFER <img style="width:20px;height:20px" src="https://earthcitizens.org/images/email.png"></a></li>
                          </ul>
                        </div>
                      </div>
                      <hr/>
                    </div>
                     <div style="width: 100%;margin-left: auto;margin-right: auto;margin-top: 0;margin-bottom: 0;max-width: 62.5rem;">
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Club Name:</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                       '.$_POST['name'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Club description</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['description'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Address 1:</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['address1'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Address 2</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['address2'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>City</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['city'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>State</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['state'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Zip Code/Postal</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['postal'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Country</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['country'].'
                    </p>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Club characteristics</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['characteristics'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Age</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['age'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Interests</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['interests'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Date created</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$d.'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Plan a meeting</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['meeting'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Website</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['website'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Facebook</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['facebook'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Contact person</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['contactperson'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Contact number</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['contactnumber'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Email address</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$_POST['emailadd'].'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Date Registered</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.date("Y-m-d").'
                    </p>
                    <hr/>
                    <label style="font:14px/20px Helvetica,Arial,sans-serif;color:#717171;padding:0 0 10px"><b>Created by</b></label>
                    <p style="font:13px/25px Helvetica,Arial,sans-serif;color:#717171;padding:0 10px 0px;margin:0px;">
                      '.$member->firstname.' '.$member->lastname.'
                    </p>
                  </div>
                </body>
                </html>
                ';

                $mail = new PHPMailer();

                $adminemail = "info@earthcitizens.org";
                $mail->isSMTP();
                $mail->Host = 'smtp.mandrillapp.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'efrenbautistajr@gmail.com';
                $mail->Password = '6Xy18AJ15My59aQNTHE5kA';               
                $mail->Port = 587;
                $mail->From = 'info@earthcitizens.org';
                $mail->FromName = 'ECO Club Request';
                $mail->addAddress($adminemail);

                $mail->isHTML(true);
                $mail->Subject = 'ECO Club Request';
                $mail->Body = $content;
                if (!$mail->send()) {
                    $data['error'] = "Email not sent";
                } else {
                    if (!$club->save()) {
                      $errors = array();
                      foreach ($club->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                      }
                      echo json_encode(array('error' => $errors));
                    } else {
                      $data['success'] = "Success";
                      echo json_encode($data);
                    }
                }

      }
                echo json_encode($data);

      

    }
  }


  public function updateAction()
  {
    if($_POST){
      $id = $_POST['id'];
      $club = Club::findFirst('id="' . $id . '"');
      $club->name = $_POST['name'];
      $club->description = $_POST['description'];
      $club->address1 = $_POST['address1'];
      $club->address2 = $_POST['address2'];
      $club->city = $_POST['city'];
      $club->state = $_POST['state'];
      $club->postal = $_POST['postal'];
      $club->country = $_POST['country'];
      $club->age = $_POST['age'];
      $club->interests = $_POST['interests'];
      $club->meeting = $_POST['meeting'];
      $club->website = $_POST['website'];
      $club->facebook = $_POST['facebook'];
      $club->contactperson = $_POST['contactperson'];
      $club->contactnumber = $_POST['contactnumber'];
      $club->emailadd = $_POST['emailadd'];
      $club->image = $_POST['image'];
      $club->date_updated = date("Y-m-d H:i:s");
      if(!$club->save()){
        $errors = array();
        foreach ($club->getMessages() as $message) {
          $errors[] = $message->getMessage();
        }
        echo json_encode(array('type' => 'danger', 'msg' => $errors));
      }else{
        echo json_encode(array('type' => 'success', 'msg' => 'Successfully Updated.'));
      }
      // echo json_encode($club->name);
    }
    else{
      echo json_encode(array('type'=>'danger','msg'=>'Something went wrong. Please try again'));
    }

  }

<<<<<<< HEAD
	public function belistAction($num, $page, $keyword, $sort, $sortto) {
		$app = new CB();
		$offsetfinal = ($page * 10) - 10;

		if ($keyword == 'undefined') {
			$conditions = "SELECT club.id,
								  club.name,
								  club.datecreated,
								  club.date_created,
								  club.website,
								  club.contactnumber,
								  club.contactperson,
								  club.createdby,
=======
  public function belistAction($num, $page, $keyword, $sort, $sortto) {
    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    if ($keyword == 'undefined') {
      $conditions = "SELECT club.id,
                  club.name,
                  club.datecreated,
                  club.date_created,
                  club.website,
                  club.contactnumber,
                  club.contactperson,
                  club.createdby,
>>>>>>> e476a4d0564884051d3ac9a5c87bdb61411397d3
                  club.status,
                  members.firstname,
                  members.lastname 
                   FROM club";
      $conditions .= " LEFT JOIN members ON club.createdby = members.userid ";
    }
    else{
      $conditions = "SELECT club.id,club.name,
                  club.datecreated,
                  club.date_created,
                  club.website,
                  club.contactnumber,
                  club.contactperson,
                  club.createdby,
                     club.status,
                  members.firstname,
                  members.lastname FROM club";
      $conditions .= " LEFT JOIN members ON club.createdby = members.userid";
      $conditions .= " WHERE name LIKE '%" . $keyword . "%' OR datecreated LIKE '%" . $keyword . "%' 
      OR date_created LIKE '%" . $keyword . "%' 
      OR contactnumber LIKE '%" . $keyword . "%'  
      OR contactperson LIKE '%" . $keyword . "%'
      OR website LIKE '%" . $keyword . "%'
      OR firstname LIKE '%" . $keyword . "%'
      OR lastname LIKE '%" . $keyword . "%' OR
      CONCAT(firstname,' ',lastname) LIKE '%" . $keyword . "%'";
    }

    if($sortto == 'DESC'){
      $sortby = "ORDER BY $sort DESC";
    }else{
      $sortby = "ORDER BY $sort ASC";
    }

    $conditions .= $sortby;

    $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

    $count = $app->dbSelect($conditions);

    echo json_encode(array(
      'data' => $searchresult,
      'index' => $page,
      'total_items' => count($count)
      ));
  }

  public function fedeleteAction($id){
    $getInfo = Club::findFirst('id="'. $id .'"');
    if ($getInfo) {
      if ($getInfo->delete()) {
        $data = array('success' => 'Club Deleted');
      }
    } 
  }

  public function viewAction($id){
    $app = new CB();
    $conditions = "SELECT club.*,members.userid,members.lastname,members.firstname 
     FROM club LEFT JOIN members ON club.createdby = members.userid where id='".$id."'";
    $searchresult = $app->dbSelect($conditions);
    if($searchresult){ echo json_encode($searchresult); }
    else{ echo json_encode("NODATA"); }
  }

  public function resolutionAction($id,$status)
  {
    $Club = Club::findFirst("id='" . $id ."'");
    if($Club){
      $Club->status = $status;
      $Club->dateupdated = date("Y-m-d H:i:s");
      if($status == 'Disapproved'){
        $Club->msg =  $_POST['msg'];
      }
      if (!$Club->save()) {
        $errors = array();
        foreach ($Club->getMessages() as $message) {
          $errors[] = $message->getMessage();
        }
        die(json_encode(array('error' => $errors)));
      } else {
        $data['success'] = "Status Updated.";
      }

        $member = Members::findFirst("userid='" . $Club->createdby ."'");

        $content = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang=en> 
        <head>
          <meta http-equiv=Content-Type content="text/html; charset=UTF-8">
          <meta name=viewport content="width=device-width, initial-scale=1">
          <!-- So that mobile will display zoomed in -->
          <meta http-equiv=X-UA-Compatible content=IE=edge>
          <!-- enable media queries for windows phone 8 -->
          <meta name=format-detection content=telephone=no>
          <!-- disable auto telephone linking in iOS -->
          <title>Earth Citizens Organization</title>
          <style type=text/css>
            body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}
            table{border-spacing: 0;}table td{border-collapse: collapse;}
            .ExternalClass{width: 100%;}
            .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
            .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
            .yshortcuts a {border-bottom: none !important;}
            @media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
            @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}
            .ios-footer a {color: #aaaaaa !important;text-decoration: underline;}
          </style>
        </head> 
        <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
          <!-- 100% background wrapper (grey background) --> 
          <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> 
            <tr>
              <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
                <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> 
                  <tr>
                    <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#5386c1;padding-left:24px;padding-right:24px"> Earth Citizens Organization </td>
                  </tr> 
                  <tr> 
                    <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
                      <div style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
                        ';

                        if($status == "Approved"){
                          $content .= '   <br/> <br/>
                          Hello '.$member->firstname.' '.$member->lastname.', <br/><br/> We are pleased to let you know, that your Club : <i>'.$Club->name.'</i> have been approved by the ECO Board.<br/><br/>';

                        }else if($status == "Disapproved"){
                          $content .= '   <br/> <br/>
                          Hello '.$member->firstname.' '.$member->lastname.', <br/><br/> We are pleased to let you know, that your Club <i>'.$Club->name.'</i> was declined by the ECO Board.
                          <br><br>
                          Note:
                          <br>
                          '.$_POST['msg'].'
                          <br>';
                        }
                        $content .=  '
                        <br><br></div>
                      </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
                      <br><br> ©2015 Earth Citizens Organization All rights reserved <br><br> <strong>Earth Citizens Organization</strong><br> <span class=ios-footer> 340 Jordan Road Sedona, AZ 86336 <br> Email: info@earthcitizens.org <br> </span>
                      <a href="https://www.earthcitizens.org" style=color:#aaaaaa>www.earthcitizens.org</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>
                      ';

        $mail = new PHPMailer();
        $adminemail = "info@earthcitizens.org";
        $mail->isSMTP();
        $mail->Host = 'smtp.mandrillapp.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'efrenbautistajr@gmail.com';
        $mail->Password = '6Xy18AJ15My59aQNTHE5kA';               
        $mail->Port = 587;

        $mail->From = 'info@earthcitizens.org';
        $mail->FromName = 'ECO Club';
        $mail->addAddress($member->email);

        $mail->isHTML(true);
        $mail->Subject = 'ECO Club';
        $mail->Body = $content;
        if (!$mail->send()) {
          $data['error'] = "Email not sent";
        } else {
            $data['success'] = "Success";
            echo json_encode($data);
        }
    }
  }


  public function showmoreclubAction($num, $page, $pageer) {

    $app = new CB();

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM club WHERE status = 'Approved' ORDER BY date_created DESC LIMIT " . $page . "," .$pageer);
    $stmt->execute();
    $info = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    $db = \Phalcon\DI::getDefault()->get('db');
    $conditions = $db->prepare("SELECT * FROM club WHERE status = 'Approved' ORDER BY date_created DESC");
    $conditions->execute();
    $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $info, 'total_items' => count($count)));
}

}

