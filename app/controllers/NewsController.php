<?php

namespace Controllers;

use \Models\News as News;
use \Models\Newscategory as Newscategory;
use \Models\Newstags as Newstags;
use \Models\Tags as Tags;
use \Models\Categories as Categories;
use \Models\Newsvideo as Newsvideo;
use \Models\Author as Author;
use \Models\Authorimage as Authorimage;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NewsController extends \Phalcon\Mvc\Controller {

    public function createNewsAction()
    {
        if($_POST){
            $title = $_POST['title'];
            $slugs = $_POST['slugs'];
            $author = $_POST['author'];
            $date = $_POST['date'];
            $summary = $_POST['summary'];
            $body = $_POST['body'];
            $imagethumb = $_POST['imagethumb'];
            $videothumb = $_POST['videothumb'];
            $status = $_POST['status'];
            $category = $_POST['category'];
            $tags = $_POST['tag'];
            $featurednews = $_POST['featurednews'];
            $feat = $_POST['feat'];
            $metatitle = $_POST['metatitle'];
            $metadesc = $_POST['metadesc'];
            $metakeyword = $_POST['metakeyword'];

            if($imagethumb != ""){
                $videothumb = "";
            }else if($videothumb != ""){
                $imagethumb = "";
            }

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates = explode(" ", $date);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

            $guid = new \Utilities\Guid\Guid();
            $newsid = $guid->GUID();

            if ($feat == 1){

                $findfeat = News::findFirst('featurednews="' . $featurednews . '" AND feat = 1');
                if ($findfeat){
                    $findfeat->feat = 0;
                    if (!$findfeat->save()) {
                        $findfeat['error'] = "Something went wrong saving page status, please try again.";
                    }else{
                        $data['success'] = "Success";

                        $savenews = new News();
                        $savenews->assign(array(
                            'newsid' => $newsid,
                            'title' => $title,
                            'newsslugs' => $slugs,
                            'summary' => $summary,
                            'author' => $author,
                            'body' => $body,
                            'imagethumb' => $imagethumb,
                            'videothumb' => $videothumb,
                            'date' => $d,
                            'status' => $status,
                            'views' => 1,
                            'type' => 'News',
                            'featurednews' => $featurednews,
                            'feat' => $feat,
                            'datecreated' => date('Y-m-d'),
                            'dateedited' =>date('Y-m-d'),
                            'metatitle' => $metatitle,
                            'metadesc' => $metadesc,
                            'metakeyword' => $metakeyword
                            ));

                        if (!$savenews->save()) {
                            $errors = array();
                            foreach ($savenews->getMessages() as $message) {
                                $errors[] = $message->getMessage();
                            }
                            echo json_encode(array('error' => $errors));
                        } else {
                            $data['success'] = "Success";

                            // $newstags = $_POST['tag'];
                            // foreach($newstags as $newstags){
                            //     $tagsofnews = new Tags();
                            //     if(isset($newstags['id'])){
                            //         $tagsofnews->assign(array(
                            //             'newsid' => $newsid,
                            //             'tags' => $newstags['id']
                            //             ));
                            //     }else{
                            //         $tagsofnews->assign(array(
                            //             'newsid' => $newsid,
                            //             'tags' => $newstags
                            //             ));
                            //     }

                            //     if (!$tagsofnews->save())
                            //     {
                            //         $data['error'] = "Something went wrong saving the data, please try again.";
                            //     }
                            //     else
                            //     {
                            //         $data['success'] = "Success Tags";
                            //     }
                            // }
                            $newstags = $tags;
                            foreach($newstags as $nt){
                                $gettags = Newstags::findFirst('tags="'.$nt.'"');
                                if(!$gettags){
                                    // var_dump($nt);
                                    $newtags = new Newstags();
                                    $newtags->assign(array(
                                        'tags' => $nt,
                                        'slugs' => str_replace("-", " ", $nt)
                                        ));

                                    if (!$newtags->save())
                                    {
                                        $data['error'] = "Something went wrong saving the newstags, please try again.";
                                    }
                                    else
                                    {
                                        $data['success'] = "Success Tags";
                                        $get = Newstags::findFirst('tags="'.$nt.'"');
                                        $tagsofnews = new Tags();
                                        $tagsofnews->assign(array(
                                            'newsid' => $newsid,
                                            'tags' => $get->id
                                            ));
                                        if (!$tagsofnews->save())
                                        {
                                            $data['error'] = "Something went wrong saving the tags, please try again.";
                                        }
                                        else
                                        {
                                            $data['success'] = "Success Tags";
                                        }
                                    }

                                }else{
                                    $data['success'] = "Success";
                                    $tagsofnews = new Tags();
                                    $tagsofnews->assign(array(
                                        'newsid' => $newsid,
                                        'tags' => $gettags->id
                                        ));
                                    if (!$tagsofnews->save())
                                    {
                                        $data['error'] = "Something went wrong saving the newstags, please try again.";
                                    }
                                    else
                                    {
                                        $data['success'] = "Success newstags else";
                                    }
                                }

                            }
                            $newscat = $_POST['category'];
                            foreach($newscat as $newscat){
                                $catofnews = new Categories();
                                if(isset($newscat['categoryid'])){
                                    $catofnews->assign(array(
                                        'newsid' => $newsid,
                                        'categoryid' => $newscat['categoryid']
                                        ));
                                }
                                else{
                                    $catofnews->assign(array(
                                        'newsid' => $newsid,
                                        'categoryid' => $newscat
                                        ));
                                }

                                if (!$catofnews->save()){
                                    $errors = array();
                                    foreach ($catofnews->getMessages() as $message) {
                                        $errors[] = $message->getMessage();
                                    }
                                    echo json_encode(array('error' => $errors));
                                }
                                else {
                                    $data['success'] = "Success Category";
                                }
                            }
                        }
                    }
                }
            }else{
                $savenews = new News();
                $savenews->assign(array(
                    'newsid' => $newsid,
                    'title' => $title,
                    'newsslugs' => $slugs,
                    'summary' => $summary,
                    'author' => $author,
                    'body' => $body,
                    'imagethumb' => $imagethumb,
                    'videothumb' => $videothumb,
                    'date' => $d,
                    'status' => $status,
                    'views' => 1,
                    'type' => 'News',
                    'featurednews' => $featurednews,
                    'feat' => 0,
                    'datecreated' => date('Y-m-d'),
                    'dateedited' =>date('Y-m-d'),
                    'metatitle' => $metatitle,
                    'metadesc' => $metadesc,
                    'metakeyword' => $metakeyword
                    ));

                if (!$savenews->save()) {
                    $errors = array();
                    foreach ($savenews->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }else{
                    $data['success'] = "Success";

                    // $newstags = $_POST['tag'];
                    // foreach($newstags as $newstags){
                    //     $tagsofnews = new Tags();
                    //     if(isset($newstags['id'])){
                    //         $tagsofnews->assign(array(
                    //             'newsid' => $newsid,
                    //             'tags' => $newstags['id']
                    //             ));
                    //     }else{
                    //         $tagsofnews->assign(array(
                    //             'newsid' => $newsid,
                    //             'tags' => $newstags
                    //             ));
                    //     }

                    //     if (!$tagsofnews->save()){
                    //         $data['error'] = "Something went wrong saving the data, please try again.";
                    //     }else{
                    //         $data['success'] = "Success Tags";
                    //     }
                    // }

                    $newstags = $tags;
                    foreach($newstags as $nt){
                        $gettags = Newstags::findFirst('tags="'.$nt.'"');
                        if(!$gettags){
                                    // var_dump($nt);
                            $newtags = new Newstags();
                            $newtags->assign(array(
                                'tags' => $nt,
                                'slugs' => str_replace("-", " ", $nt)
                                ));

                            if (!$newtags->save())
                            {
                                $data['error'] = "Something went wrong saving the newstags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success Tags";
                                $get = Newstags::findFirst('tags="'.$nt.'"');
                                $tagsofnews = new Tags();
                                $tagsofnews->assign(array(
                                    'newsid' => $newsid,
                                    'tags' => $get->id
                                    ));
                                if (!$tagsofnews->save())
                                {
                                    $data['error'] = "Something went wrong saving the tags, please try again.";
                                }
                                else
                                {
                                    $data['success'] = "Success Tags";
                                }
                            }

                        }else{
                            $data['success'] = "Success";
                            $tagsofnews = new Tags();
                            $tagsofnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $gettags->id
                                ));
                            if (!$tagsofnews->save())
                            {
                                $data['error'] = "Something went wrong saving the newstags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success newstags else";
                            }
                        }

                    }

                    $newscat = $_POST['category'];
                    foreach($newscat as $newscat){
                    $catofnews = new Categories();
                    if(isset($newscat['categoryid'])){
                        $catofnews->assign(array(
                            'newsid' => $newsid,
                            'categoryid' => $newscat['categoryid']
                            ));
                    }
                    else{
                        $catofnews->assign(array(
                            'newsid' => $newsid,
                            'categoryid' => $newscat
                            ));
                    }

                    if (!$catofnews->save()){
                        $errors = array();
                        foreach ($catofnews->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }
                    else {
                        $data['success'] = "Success Category";
                    }
                }
            }
        }
    }
    echo json_encode($data);
}

public function newsUpdateAction()
{
    var_dump($_POST);

    if($_POST){

        $newsid = $_POST['newsid'];
        $title = $_POST['title'];
        $slugs = $_POST['slugs'];
        $author = $_POST['author'];
        $date = $_POST['date'];
        $summary = $_POST['summary'];
        $body = $_POST['body'];
        $imagethumb = $_POST['imagethumb'];
        $videothumb = $_POST['videothumb'];
        $category = $_POST['category'];
        $tags = $_POST['tag'];
        $featurednews = $_POST['featurednews'];
        $feat = $_POST['feat'];
        $metatitle = $_POST['metatitle'];
        $metadesc = $_POST['metadesc'];
        $metakeyword = $_POST['metakeyword'];
        $statuschk = $_POST['status'];


        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
        $dates = explode(" ", $date);
        $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

        if($imagethumb != ""){
            $videothumb = "";
        }else if($videothumb != ""){
            $imagethumb = "";
        }

        if($statuschk == "true"){
            $status = 1;
        }else if($statuschk == "draft"){
            $status = 2;
        }else{
            $status = 0;
        }

        if ($feat == 1){
            $findfeat = News::findFirst('featurednews="' . $featurednews . '" AND feat = 1');
            if ($findfeat){
                $findfeat->feat = 0;
                if (!$findfeat->save()) {
                    $findfeat['error'] = "Something went wrong saving page status, please try again.";
                } else {
                    $data['success'] = "Success";

                    $newStr = trim($title, '"');
                    $title2 = str_replace('\"', '', $newStr);

                    $titlenews = News::findFirst('title LIKE "%'.$title2.'%"');
                    $idnews = News::findFirst('newsid="'.$newsid.'" AND title LIKE "%'.$title2.'%"');

                    if ($titlenews == true && $idnews == false ) {
                        die(json_encode(array('newsalreadyexist' => 'Title is already existing')));
                    }


                    $news = News::findFirst("newsid='" . $newsid ."'");
                    $news->title = $_POST['title'];
                    $news->newsslugs = $_POST['slugs'];
                    $news->author = $_POST['author'];
                    $news->summary = $_POST['summary'];
                    $news->body = $_POST['body'];
                    $news->imagethumb = $imagethumb;
                    $news->videothumb = $videothumb;
                    $news->status = $status;
                    $news->featurednews = $featurednews;
                    $news->feat = $feat;
                    $news->dateedited = date('Y-m-d');
                    $news->date = $d;
                    $news->metatitle = $metatitle;
                    $news->metadesc = $metadesc;
                    $news->metakeyword = $metakeyword;

                    if (!$news->save()) {
                        $errors = array();
                        foreach ($news->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    } else {
                        $conditions = 'newsid="' . $newsid . '"';
                        $deletenewstags = Tags::find(array($conditions));
                        $data = array('error' => 'Not Found');
                        if ($deletenewstags) {
                            foreach($deletenewstags as $deletenewstags){

                                if ($deletenewstags->delete()) {
                                    $data = array('success' => 'tags Deleted');
                                }
                            }
                        }

                        $newstags = $tags;
                        foreach($newstags as $nt){
                            $gettags = Newstags::findFirst('tags="'.$nt.'"');
                            if(!$gettags){
                                $newtags = new Newstags();
                                $newtags->assign(array(
                                    'tags' => $nt,
                                    'slugs' => str_replace("-", " ", $nt)
                                    ));

                                if (!$newtags->save())
                                {
                                    $data['error'] = "Something went wrong saving the newstags, please try again.";
                                }
                                else
                                {
                                    $data['success'] = "Success Tags";
                                    $get = Newstags::findFirst('tags="'.$nt.'"');
                                    $tagsofnews = new Tags();
                                    $tagsofnews->assign(array(
                                        'newsid' => $newsid,
                                        'tags' => $get->id
                                        ));
                                    if (!$tagsofnews->save())
                                    {
                                        $data['error'] = "Something went wrong saving the tags, please try again.";
                                    }
                                    else
                                    {
                                        $data['success'] = "Success Tags";
                                    }
                                }

                            }else{
                                $data['success'] = "Success";
                                $tagsofnews = new Tags();
                                $tagsofnews->assign(array(
                                    'newsid' => $newsid,
                                    'tags' => $gettags->id
                                    ));
                                if (!$tagsofnews->save())
                                {
                                    $data['error'] = "Something went wrong saving the newstags, please try again.";
                                }
                                else
                                {
                                    $data['success'] = "Success newstags else";
                                }
                            }

                        }

                        $conditionscat = 'newsid="' . $newsid . '"';
                        $deletenewscat = Categories::find(array($conditionscat));
                        $data = array('error' => 'Not Found');
                        if ($deletenewscat) {
                            foreach($deletenewscat as $deletenewscat){

                                if ($deletenewscat->delete()) {
                                    $data = array('success' => 'tags Deleted');
                                }
                            }
                        }

                        $newscat = $_POST['category'];
                        foreach($newscat as $newscat){
                            $catofnews = new Categories();
                            if(isset($newscat['categoryid'])){
                                $catofnews->assign(array(
                                    'newsid' => $newsid,
                                    'categoryid' => $newscat['categoryid']
                                    ));
                            }
                            else{
                                $catofnews->assign(array(
                                    'newsid' => $newsid,
                                    'categoryid' => $newscat
                                    ));
                            }

                            if (!$catofnews->save()){
                                $errors = array();
                                foreach ($catofnews->getMessages() as $message) {
                                    $errors[] = $message->getMessage();
                                }
                                echo json_encode(array('error' => $errors));
                            }
                            else {
                                $data['success'] = "Success Category";
                            }
                        }
                    }

                }
            }
        }
        else{

            $newStr = trim($title, '"');
            $title2 = str_replace('\"', '', $newStr);

            $titlenews = News::findFirst('title="'.$title2.'"');
            $idnews = News::findFirst('newsid="'.$newsid.'" AND title="'.$title2.'"');

            if ($titlenews == true && $idnews == false ) {
                die(json_encode(array('newsalreadyexist' => 'Title is already existing')));
            }

            $news = News::findFirst("newsid='" . $newsid ."'");
            $news->title = $title;
            $news->newsslugs = $slugs;
            $news->author = $author;
            $news->summary = $summary;
            $news->body = $body;
            $news->imagethumb = $imagethumb;
            $news->videothumb = $videothumb;
            $news->status = $status;
            $news->featurednews = $featurednews;
            $news->feat = $feat;
            $news->dateedited = date('Y-m-d');
            $news->date = $d;
            $news->metatitle = $metatitle;
            $news->metadesc = $metadesc;
            $news->metakeyword = $metakeyword;

            if (!$news->save()) {
                $errors = array();
                foreach ($news->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $conditions = 'newsid="' . $newsid . '"';
                $deletenewstags = Tags::find(array($conditions));
                $data = array('error' => 'Not Found');
                if ($deletenewstags) {
                    foreach($deletenewstags as $deletenewstags){

                        if ($deletenewstags->delete()) {
                            $data = array('success' => 'tags Deleted');
                        }
                    }
                }
                // $newstags = $tags;
                // foreach($newstags as $newstags){
                //     $tagsofnews = new Tags();
                //     if(isset($newstags['id'])){
                //         $tagsofnews->assign(array(
                //             'newsid' => $newsid,
                //             'tags' => $newstags['id']
                //             ));
                //     }else{
                //         $tagsofnews->assign(array(
                //             'newsid' => $newsid,
                //             'tags' => $newstags
                //             ));
                //     }

                //     if (!$tagsofnews->save())
                //     {
                //         $data['error'] = "Something went wrong saving the data, please try again.";
                //     }
                //     else
                //     {
                //         $data['success'] = "Success Tags";
                //     }
                // }
                $newstags = $tags;
                foreach($newstags as $nt){
                    $gettags = Newstags::findFirst('tags="'.$nt.'"');
                    if(!$gettags){
                                    // var_dump($nt);
                        $newtags = new Newstags();
                        $newtags->assign(array(
                            'tags' => $nt,
                            'slugs' => str_replace("-", " ", $nt)
                            ));

                        if (!$newtags->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $data['success'] = "Success Tags";
                            $get = Newstags::findFirst('tags="'.$nt.'"');
                            $tagsofnews = new Tags();
                            $tagsofnews->assign(array(
                                'newsid' => $newsid,
                                'tags' => $get->id
                                ));
                            if (!$tagsofnews->save())
                            {
                                $data['error'] = "Something went wrong saving the tags, please try again.";
                            }
                            else
                            {
                                $data['success'] = "Success Tags";
                            }
                        }

                    }else{
                        $data['success'] = "Success";
                        $tagsofnews = new Tags();
                        $tagsofnews->assign(array(
                            'newsid' => $newsid,
                            'tags' => $gettags->id
                            ));
                        if (!$tagsofnews->save())
                        {
                            $data['error'] = "Something went wrong saving the newstags, please try again.";
                        }
                        else
                        {
                            $data['success'] = "Success newstags else";
                        }
                    }

                }

                $conditionscat = 'newsid="' . $newsid . '"';
                $deletenewscat = Categories::find(array($conditionscat));
                $data = array('error' => 'Not Found');
                if ($deletenewscat) {
                    foreach($deletenewscat as $deletenewscat){

                        if ($deletenewscat->delete()) {
                            $data = array('success' => 'tags Deleted');
                        }
                    }
                }

                $newscat = $_POST['category'];
                foreach($newscat as $newscat){
                    $catofnews = new Categories();
                    if(isset($newscat['categoryid'])){
                        $catofnews->assign(array(
                            'newsid' => $newsid,
                            'categoryid' => $newscat['categoryid']
                            ));
                    }
                    else{
                        $catofnews->assign(array(
                            'newsid' => $newsid,
                            'categoryid' => $newscat
                            ));
                    }

                    if (!$catofnews->save()){
                        $errors = array();
                        foreach ($catofnews->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        echo json_encode(array('error' => $errors));
                    }
                    else {
                        $data['success'] = "Success Category";
                    }
                }
            }

        }
    }

    echo json_encode($data);
}


public function newsinfoAction($newsid) {

    $data = array();
    $news = News::findFirst('newsid="' . $newsid . '"');
    if ($news) {
        $dbtags = \Phalcon\DI::getDefault()->get('db');
        $gettags = $dbtags->prepare("SELECT id, newstags.tags FROM newstags INNER JOIN tags ON newstags.id = tags.tags WHERE tags.newsid = '" . $newsid . "'");
        $gettags->execute();
        $tags = $gettags->fetchAll(\PDO::FETCH_ASSOC);

        $dbcat = \Phalcon\DI::getDefault()->get('db');
        $getcat = $dbcat->prepare("SELECT * FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid = '" . $newsid . "'");
        $getcat->execute();
        $cat = $getcat->fetchAll(\PDO::FETCH_ASSOC);

        $data = array(
            'newsid' => $news->newsid,
            'title' => $news->title,
            'slugs' => $news->newsslugs,
            'author' => $news->author,
            'summary' => $news->summary,
            'body' => $news->body,
            'imagethumb' => $news->imagethumb,
            'videothumb' => $news->videothumb,
            'status' => $news->status,
            'featurednews' => $news->featurednews,
            'feat' => $news->feat,
            'date' => $news->date,
            'datecreated' => $news->datecreated,
            'metatitle' => $news->metatitle,
            'metadesc' => $news->metadesc,
            'metakeyword' => $news->metakeyword,
            'tag' => $tags,
            'category' => $cat

            );
    }
    echo json_encode($data);
}


public function checknewstitlesAction($newstitle) {
    $data = array();
    if($newstitle){
        $newStr = trim($newstitle, '"');
        $title2 = str_replace('\"', '', $newStr);
        $titlenews = News::findFirst('title="'.$title2.'"');

        if($titlenews){
            die(json_encode(array('error' => array('newsalreadyexist' => 'Title is already existing'))));
        }
    }
}

public function checkcattitlesAction($cattitle) {
    $data = array();
    $titlecat = Newscategory::findFirst('categoryname LIKE "' . $cattitle . '"');
    if ($titlecat == true) {
        ($titlecat == true) ? $data["categoryexist"] = "The news category is already exist!" : '';
    }else{
        ($titlecat == false) ? $data["categoryavailable"] = "The news category are available." : '';
    }
    echo json_encode($data);
}

public function checktagstitlesAction($tagstitle) {
    $data = array();
    $titletag = Newstags::findFirst("tags LIKE '" . $tagstitle . "'");
    if ($titletag == true) {
        ($titletag == true) ? $data["tagsexist"] = "The news tag is already exist!" : '';
    }else{
        ($titletag == false) ? $data["tagsavailable"] = "The news tag are available." : '';
    }
    echo json_encode($data);
}


public function managenewsAction($num, $page, $keyword, $sortingdate, $sort, $sortto) {

    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
    $dates = explode(" ", $sortingdate);
    $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

    if($sortingdate == 'undefined'){
        $sortingdate = 'null';
    }
    if($keyword == 'undefined'){
        $keyword = 'null';
    }

    if ($keyword == 'null' && $sortingdate == 'null') {

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid ";

    }elseif ($keyword != 'null' && $sortingdate != 'null'){

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE title LIKE '%". $keyword ."%'";
        $conditions .= " OR author.name LIKE '%". $keyword ."%' AND news.date LIKE '". $d ."' ";

    }elseif ($keyword == 'null' && $sortingdate != 'null'){

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE news.date LIKE '". $d ."' ";

    }else {

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE title LIKE '%". $keyword ."%'";
        $conditions .= " OR author.name LIKE '%". $keyword ."%' ";

    }

    if($sortto == 'DESC'){
        $sortby = "ORDER BY $sort DESC";
    }else{
        $sortby = "ORDER BY $sort ASC";
    }

    $conditions .= $sortby;

    $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

    $count = $app->dbSelect($conditions);

    echo json_encode(array(
        'data' => $searchresult,
        'index' => $page,
        'total_items' => count($count)
        ));
}


public function managebyfeatcatAction($num, $page, $keyword, $sortingdate, $sort, $sortto) {

    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
    $dates = explode(" ", $sortingdate);
    $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

    if($sortingdate == 'undefined'){
        $sortingdate = 'null';
    }
    if($keyword == 'undefined' || $keyword == ''){
        $keyword = 'null';
    }

    if ($keyword == 'null' && $sortingdate == 'null' ) {

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE featurednews >= 1 ";

    }elseif ($keyword != 'null' && $sortingdate != 'null'){

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE featurednews >= 1";
        $conditions .= " AND news.date LIKE '". $d ."' AND featurednews = $keyword ";

    }elseif ($keyword == 'null' && $sortingdate != 'null'){

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE featurednews >= 1";
        $conditions .= " AND news.date LIKE '". $d ."' ";

    }else {

        $conditions = "SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE featurednews >= 1";
        $conditions .= " AND title LIKE '". $keyword ."%' OR featurednews = $keyword  OR author.name LIKE '%". $keyword ."%' ";

    }
    if($sortto == 'DESC'){
        $sortby = "ORDER BY featurednews ASC, feat DESC, $sort DESC";
    }else{
        $sortby = "ORDER BY featurednews ASC, feat DESC, $sort ASC";
    }

    $conditions .= $sortby;

    $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

    $count = $app->dbSelect($conditions);

    echo json_encode(array(
        'data' => $searchresult,
        'index' => $page,
        'total_items2' => count($count)
        ));
}


public function showNewsAction($num, $page, $pageer) {

    $app = new CB();

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC LIMIT " . $page . "," .$pageer);
    $stmt->execute();
    $newsinfo = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    foreach($newsinfo as $sr=>$val){
        $categories = $app->dbSelect("SELECT categoryname, categoryslugs FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid ='".$val['newsid']."' ");
        $newsinfo[$sr]['categories'] = $categories;
    }

    $db = \Phalcon\DI::getDefault()->get('db');
    $conditions = $db->prepare("SELECT * FROM news LEFT JOIN  author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC");
    $conditions->execute();
    $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $newsinfo, 'total_items' => count($count)));
}


public function showlistnewsbycategoryAction($num, $page, $pageer, $categoryslugs){

    $app = new CB();

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM categories LEFT JOIN news ON categories.newsid=news.newsid LEFT JOIN newscategory ON categories.categoryid=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE newscategory.categoryslugs LIKE '". $categoryslugs ."' ORDER BY date DESC LIMIT " . $page . "," .$pageer);
    $stmt->execute();
    $newsinfo = $stmt->fetchAll(\PDO::FETCH_ASSOC);


    foreach($newsinfo as $sr=>$val){
        $categories = $app->dbSelect("SELECT categoryname, categoryslugs FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid ='".$val['newsid']."' ");
        $newsinfo[$sr]['categories'] = $categories;
    }

    $db = \Phalcon\DI::getDefault()->get('db');
    $conditions = $db->prepare("SELECT * FROM categories LEFT JOIN news ON categories.newsid=news.newsid LEFT JOIN newscategory ON categories.categoryid=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE newscategory.categoryslugs LIKE '". $categoryslugs ."' ORDER BY date DESC");
    $conditions->execute();
    $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $newsinfo, 'total_items' => count($count)));
}

public function showlistfeaturedcategoryAction($num, $page, $pageer, $categoryslugs){

    $app = new CB();

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE featurednews='" .$categoryslugs. "' AND status = 1 ORDER BY date DESC LIMIT " . $page . "," .$pageer);
    $stmt->execute();
    $newsinfo = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    foreach($newsinfo as $sr=>$val){
        $categories = $app->dbSelect("SELECT categoryname, categoryslugs FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid ='".$val['newsid']."' ");
        $newsinfo[$sr]['categories'] = $categories;
    }

    $db = \Phalcon\DI::getDefault()->get('db');
    $conditions = $db->prepare("SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE featurednews='" .$categoryslugs. "' AND status = 1 ORDER BY date DESC");
    $conditions->execute();
    $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $newsinfo, 'total_items' => count($count)));
}


public function showlistnewsbytagsAction($num, $page, $pageer, $tagname){

    $app = new CB();

    $db1 = \Phalcon\DI::getDefault()->get('db');
    $stmt1 = $db1->prepare("SELECT * FROM tags INNER JOIN news ON tags.newsid=news.newsid LEFT JOIN newstags ON tags.tags=newstags.id LEFT JOIN author ON news.author=author.authorid WHERE newstags.slugs LIKE '". $tagname ."' ORDER BY date DESC LIMIT " . $page . "," .$pageer);
    $stmt1->execute();
    $newstags = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

    foreach($newstags as $sr=>$val){
        $categories = $app->dbSelect("SELECT categoryname, categoryslugs FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid ='".$val['newsid']."' ");
        $newstags[$sr]['categories'] = $categories;
    }

    $db = \Phalcon\DI::getDefault()->get('db');
    $conditions = $db->prepare("SELECT * FROM tags INNER JOIN news ON tags.newsid=news.newsid LEFT JOIN newstags ON tags.tags=newstags.id LEFT JOIN author ON news.author=author.authorid WHERE newstags.slugs LIKE '". $tagname ."' ORDER BY date DESC");
    $conditions->execute();
    $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $newstags, 'total_items' => count($count)));
}

public function showlistofauthorNewsAction($num, $page, $pageer, $authorid) {

    $app = new CB();

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE authorid='" .$authorid. "' AND status = 1 ORDER BY date DESC LIMIT " . $page . "," .$pageer);
    $stmt->execute();
    $newsinfo = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    foreach($newsinfo as $sr=>$val){
        $categories = $app->dbSelect("SELECT categoryname, categoryslugs FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid ='".$val['newsid']."' ");
        $newsinfo[$sr]['categories'] = $categories;
    }

    $db = \Phalcon\DI::getDefault()->get('db');
    $conditions = $db->prepare("SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE authorid='" .$authorid. "' AND status = 1 ORDER BY date DESC");
    $conditions->execute();
    $count = $conditions->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $newsinfo, 'total_items' => count($count)));
}


public function managecategoryAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Pages = Newscategory::find();
    } else {
        $conditions = "categoryname LIKE '%" . $keyword . "%'";
        $Pages = Newscategory::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Pages,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'categoryid' => $m->categoryid,
            'categoryname' => $m->categoryname,
            'categoryslugs' => $m->categoryslugs,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}



public function listcategoryAction()
{

    $getcategory= Newscategory::find(array("order" => "categoryid DESC"));
    foreach ($getcategory as $getcategory) {;
        $data[] = array(
            'categoryid'=>$getcategory->categoryid,
            'categoryname'=>$getcategory->categoryname,
            'categoryslugs'=>$getcategory->categoryslugs
            );
    }
    echo json_encode($data);

}

public function listtagsAction()
{

    $gettags= Newstags::find(array("order" => "id DESC"));
    foreach ($gettags as $gettags) {;
        $data[] = array(
            'id'=>$gettags->id,
            'tags'=>$gettags->tags,
            'slugs'=>$gettags->slugs
            );
    }
    echo json_encode($data);

}

public function listauthorAction()
{

    $getauthor= Author::find(array("order" => "num DESC"));
    foreach ($getauthor as $getauthor) {;
        $data[] = array(
            'authorid'=>$getauthor->authorid,
            'name'=>$getauthor->name,
            'location'=>$getauthor->location,
            'occupation'=>$getauthor->occupation,
            'about'=>$getauthor->about,
            'image'=>$getauthor->image
            );
    }
    echo json_encode($data);

}

public function listauthorimagesAction()
{

    $getauthorimage= Authorimage::find(array("order" => "id DESC"));
    foreach ($getauthorimage as $getauthorimage) {;
        $data[] = array(
            'id'=>$getauthorimage->id,
            'filename'=>$getauthorimage->filename
            );
    }
    echo json_encode($data);

}


public function categorydeleteAction($categoryid) {
    $conditions = "categoryid=" . $categoryid;
    $news = Newscategory::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'Category Deleted');
        }
    }
    echo json_encode($data);
}


public function newslistAction() {

    $newslist = News::find(array("order" => "date DESC"));
    foreach ($newslist as $newslist) {
        $data[] = array(
            'newsid' => $newslist->newsid,
            'title' => $newslist->title,
            'body' => $newslist->body,
            'banner' => $newslist->banner,
            'check' => $newslist->status,
            'date' => $newslist->date,
            'slugs' => $newslist->newsslugs
            );
    }
    echo json_encode($data);

}


public function categoryUpdateAction()
{

    if($_POST){
        $id = $_POST["id"];
        $catname = $_POST["catnames"];
        $catslugs = $_POST["catslugs"];

        $find = Newscategory::findFirst('categoryid!=' . $id . ' AND categoryname="'.$catname.'" ');
        if($find){
            $data['error'] = "Your data already exist.";
            echo json_encode($data);
        }else{
            $data = array();
            $news = Newscategory::findFirst('categoryid=' . $id . '');
            $news->categoryname = $catname;
            $news->categoryslugs = $catslugs;

            if (!$news->save())
            {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
            else
            {
                $data['success'] = "Success";
            }
            echo json_encode($data);
        }
    }
}

public function createtagsAction()
{

   $data = array();

   $tagsnames = new Newstags();
   $tagsnames->assign(array(
    'tags' => $_POST['tags'],
    'slugs' => $_POST['slugs'],
    ));
   if (!$tagsnames->save()){
    $errors = array();
    foreach ($tagsnames->getMessages() as $message) {
        $errors[] = $message->getMessage();
    }
    echo json_encode(array('error' => $errors));
}
else{
 $data['success'] = "Success";
}
echo json_encode($data);
}


public function showlistNewsAction($num, $page, $pageer) {

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author ON news.author=author.authorid WHERE status = 1 ORDER BY date DESC LIMIT " . $page . "," .$pageer);
    $stmt->execute();
    $newsinfo = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode(array('data' => $newsinfo));
}


public function fullnewsAction($newsslugs) {

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid LEFT JOIN author ON news.author=author.authorid WHERE newsslugs = '". $newsslugs ."' and status = 1");
    $stmt->execute();
    $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode($searchresult);

}


public function metaauthorAction($authorid) {

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM author WHERE authorid = '". $authorid ."'");
    $stmt->execute();
    $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode($searchresult);

}

public function viewfronenttagsAction($newsslugs) {

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM tags LEFT JOIN news ON tags.newsid=news.newsid LEFT JOIN newstags ON tags.tags=newstags.id WHERE news.newsslugs = '". $newsslugs ."' ORDER BY date DESC");
    $stmt->execute();
    $newstags = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode($newstags);

}

public function viewfronentcategoryAction($newsslugs) {

    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM categories LEFT JOIN news ON categories.newsid=news.newsid LEFT JOIN newscategory ON categories.categoryid=newscategory.categoryid WHERE news.newsslugs = '". $newsslugs ."' ORDER BY date DESC");
    $stmt->execute();
    $newscat = $stmt->fetchAll(\PDO::FETCH_ASSOC);

    echo json_encode($newscat);

}

public function createAuthorAction() {

    if($_POST){

        $name = $_POST['name'];
        $about = $_POST['about'];
        $location = $_POST['location'];
        $occupation = $_POST['occupation'];
        $photo = $_POST['photo'];
        $authorsince = $_POST['authorsince'];

        $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
        $dates = explode(" ", $authorsince);
        $since = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

        $guid = new \Utilities\Guid\Guid();
        $id = $guid->GUID();

        $author = new Author();
        $author->assign(array(
            'authorid' => $id,
            'name' => $name,
            'location' => $location,
            'occupation' => $occupation,
            'about' => $about,
            'image' => $photo,
            'authorsince' => $since,
            'date_created' => date('Y-m-d'),
            'date_updated' => date('Y-m-d')
            ));

        if (!$author->save()) {
            $errors = array();
            foreach ($author->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        } else {
            $data['success'] = "Success";
        }

    }

    echo json_encode($data);
}

public function manageAuthorAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $Author = Author::find();
    } else {
        $conditions = "name LIKE '%" . $keyword . "%' OR location LIKE '%" . $keyword . "%' OR occupation LIKE '%" . $keyword . "%' OR authorsince LIKE '%" . $keyword . "%'";
        $Author = Author::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $Author,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'authorid' => $m->authorid,
            'name' => $m->name,
            'location' => $m->location,
            'occupation' => $m->occupation,
            'about' => $m->about,
            'authorsince' => $m->authorsince,
            'date_created' => $m->date_created,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}

public function authoreditoAction($authorid) {
    $data = array();

    $author = Author::findFirst('authorid="' . $authorid . '"');
    if ($author) {
        $data = array(
            'authorid' => $author->authorid,
            'name' => $author->name,
            'location' => $author->location,
            'occupation' => $author->occupation,
            'about' => $author->about,
            'photo' => $author->image,
            'authorsince' => $author->authorsince,
            'date_created' => $author->date_created

            );
    }
    echo json_encode($data);
}

public function authorupdateAction() {

 $request = new \Phalcon\Http\Request();

 if($request->isPost()){

    $authorid = $request->getPost('authorid');
    $name = $request->getPost('name');
    $location = $request->getPost('location');
    $occupation = $request->getPost('occupation');
    $about = $request->getPost('about');
    $photo = $request->getPost('photo');
    $authorsince = $request->getPost('authorsince');

    $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
    $dates = explode(" ", $authorsince);
    $since = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

    $author = Author::findFirst('authorid="' . $authorid . '"');
    $author->name = $name;
    $author->location = $location;
    $author->occupation = $occupation;
    $author->about = $about;
    $author->image = $photo;
    $author->authorsince = $since;

    if (!$author->save()) {
        $data['error'] = "Something went wrong saving the data, please try again.";
    } else {
        $data['success'] = "Success";
    }
    echo json_encode($data);
}
}


public function authordeleteAction($authorid) {

    $conditions = 'authorid="' . $authorid . '"';
    $author = Author::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($author) {
        if ($author->delete()) {
            $data = array('success' => 'Author Deleted');
        }
    }
    echo json_encode($data);
}



public function newsdeleteAction($newsid) {
    $conditions = 'newsid="' . $newsid . '"';
    $news = News::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'News Deleted');
        }
    }
    $dltcat = Categories::find("newsid='" . $newsid ."'");
    if ($dltcat) {
        foreach($dltcat as $dltcat){

            if ($dltcat->delete()) {
                $data = array('success' => 'Deleted');
            }
        }
    }
    $dlttags = Tags::find("newsid='" . $newsid ."'");
    if ($dlttags) {
        foreach($dlttags as $dlttags){

            if ($dlttags->delete()) {
                $data = array('success' => 'Deleted');
            }
        }
    }
    echo json_encode($data);
}


public function newsUpdatestatusAction($status,$newsid,$keyword) {

    $data = array();
    $news = News::findFirst('newsid="' . $newsid . '"');
    $news->status = $status;
    if (!$news->save()) {
        $news['error'] = "Something went wrong saving page status, please try again.";
    } else {
        $data['success'] = "Success";
    }

    echo json_encode($data);
}


public function updatenewsfeatstatusAction($feat,$featurednews,$newsid,$keyword) {

    $data = array();
    $findfeat = News::findFirst('featurednews="' . $featurednews . '" AND feat = 1');
    if ($findfeat ){
        $findfeat->feat = 0;
        if (!$findfeat->save()) {
            $findfeat['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";

            $news = News::findFirst('newsid="' . $newsid . '"');
            $news->feat = 1;
            if (!$news->save()) {
                $news['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
    }else{
        $news = News::findFirst('newsid="' . $newsid . '"');
        $news->feat = 1;
        if (!$news->save()) {
            $news['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }

    echo json_encode($data);
}


public function getauthorbyidAction($authorid){
    $data = array();
    $author = Author::findFirst('authorid="' . $authorid . '"');
    if ($author) {
        $data = array(
            'authorid' => $author->authorid,
            'name' => $author->name,
            'location' => $author->location,
            'occupation' => $author->occupation,
            'about' => $author->about,
            'photo' => $author->image,
            'date_created' => $author->date_created

            );
    }
    echo json_encode($data);
}

public function getcategoryfrontendAction(){
    $data = array();

    $getcategory= Newscategory::find(array("order" => "categoryid DESC"));
    foreach ($getcategory as $getcategory) {;
        $data[] = array(
            'categoryid'=>$getcategory->categoryid,
            'categoryname'=>$getcategory->categoryname,
            'categoryslugs'=>$getcategory->categoryslugs
            );
    }
    echo json_encode($data);
}

public function getcategorynamefrontendAction($catname){
    $data = array();
    $getcategoryname= Newscategory::findFirst('categoryslugs="' . $catname . '"');
    if ($getcategoryname) {
        $data = array(
            'categoryid' => $getcategoryname->categoryid,
            'categoryname' => $getcategoryname->categoryname,
            'categoryslugs' => $getcategoryname->categoryslugs

            );
    }
    echo json_encode($data);
}

public function gettagnamefrontendAction($tagname){
    $data = array();
    $gettagname= Newstags::findFirst('slugs="' . $tagname . '"');
    if ($gettagname) {
        $data = array(
            'id' => $gettagname->id,
            'tags' => $gettagname->tags,
            'slugs' => $gettagname->slugs

            );
    }
    echo json_encode($data);
}


public function createcategoryAction()
{

 $data = array();

 $catnames = new Newscategory();
 $catnames->assign(array(
    'categoryname' => $_POST['catnames'],
    'categoryslugs' => $_POST['slugs'],
    ));
 if (!$catnames->save()){
    $errors = array();
    foreach ($catnames->getMessages() as $message) {
        $errors[] = $message->getMessage();
    }
    echo json_encode(array('error' => $errors));
}
else{
   $data['success'] = "Success";
}
echo json_encode($data);
}


public function managetagsAction($num, $page, $keyword) {

    if ($keyword == 'null' || $keyword == 'undefined') {
        $tag = Newstags::find();
    } else {
        $conditions = "tags LIKE '%" . $keyword . "%'";
        $tag = Newstags::find(array($conditions));
    }

    $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
    $paginator = new \Phalcon\Paginator\Adapter\Model(
        array(
            "data" => $tag,
            "limit" => 10,
            "page" => $currentPage
            )
        );

        // Get the paginated results
    $page = $paginator->getPaginate();

    $data = array();
    foreach ($page->items as $m) {
        $data[] = array(
            'id' => $m->id,
            'tags' => $m->tags,
            'slugs' => $m->slugs,
            );
    }
    $p = array();
    for ($x = 1; $x <= $page->total_pages; $x++) {
        $p[] = array('num' => $x, 'link' => 'page');
    }
    echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

}

public function tagsdeleteAction($id) {
    $conditions = "id=" . $id;
    $news = Newstags::findFirst(array($conditions));
    $data = array('error' => 'Not Found');
    if ($news) {
        if ($news->delete()) {
            $data = array('success' => 'Category Deleted');
        }
    }
    echo json_encode($data);
}

public function updatetagsAction()
{
// var_dump($_POST);
    if($_POST){
        $id = $_POST['id'];

        $find = Newstags::findFirst('id!=' . $id . ' AND tags = "'.$_POST['tags'].'"');
        if($find){
            $data['error'] = "Your data already exist.";
            echo json_encode($data);
        }else {
            $data = array();
            $news = Newstags::findFirst('id=' . $id . ' ');
            $news->tags = $_POST['tags'];
            $news->slugs = $_POST['slugs'];

            if (!$news->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
            echo json_encode($data);
        }
    }


}

public function newrssAction($offset) {
    $news = News::find(array("status = 1 order"=>"date DESC"));
    $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
    echo $postinfo;
}

public function featuredblogAction($num) {
    $get= News::findFirst('featurednews="' . $num . '" AND feat = 1');
    if ($get) {
        $data = array(
            'title' => $get->title,
            'newsslugs' => $get->newsslugs,
            'summary' => $get->summary,
            'imagethumb' => $get->imagethumb,
            'videothumb' => $get->videothumb

            );
    }
    echo json_encode($data);
}

public function latestNewsAction() {
    $app = new CB();
    $getLatest = "SELECT * FROM news";
    $getLatest .= " ORDER BY date DESC LIMIT 1";
    $data = $app->dbSelect($getLatest);
    echo json_encode($data);
}



public function TagsListAction(){    
    $newscategory = Newstags::find(array());
    $newscategory = json_encode($newscategory->toArray(), JSON_NUMERIC_CHECK);
    echo $newscategory;
}
public function ArchiveListAction() {
    $app = new CB();

    $conditions = "SELECT * FROM news ";
    $conditions .= "WHERE status = '1' ";
    $conditions .= "ORDER BY date DESC";
    $searchresult = $app->dbSelect($conditions);
    $dates = [];
    foreach($searchresult as $sr=>$val){
        if(!in_array(date('F Y', strtotime($val['date'])), $dates)){

            $data[] = array(
                'month' => date('F', strtotime($val['date'])),
                'year' => date('Y', strtotime($val['date'])),
                'datepublished' => $val['date']
                );
            array_push($dates, date('F Y', strtotime($val['date'])));
        }
    }
    echo json_encode($data);

}

public function listnewsbyarchiveAction($num, $offset, $page, $month, $year) {
    $searchdate = date("Y-m-d", strtotime($month." ".$year));
    $betweendate = date("Y-m-d", strtotime("+1 months -1 day", strtotime($searchdate)));
    $app = new CB();
    $db = \Phalcon\DI::getDefault()->get('db');
    $stmt = $db->prepare("SELECT * FROM news LEFT JOIN author on news.author = author.authorid WHERE news.date >= '$searchdate' AND news.date <= '$betweendate' AND news.status = 1 ORDER BY news.date DESC LIMIT ".$offset.','. $page);

    $stmt->execute();
    $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    foreach($searchresult as $sr=>$val){
        $categories = $app->dbSelect("SELECT newscategory.categoryid, newscategory.categoryname, newscategory.categoryslugs FROM newscategory INNER JOIN categories ON newscategory.categoryid = categories.categoryid WHERE categories.newsid ='".$val['newsid']."'");
        $searchresult[$sr]['category'] = $categories;
    }

    $count = $app->dbSelect("SELECT * FROM news LEFT JOIN author on news.author = author.authorid WHERE news.date >= '$searchdate' AND news.date <= '$betweendate' AND news.status = 1");
        
    $data['data'] = $searchresult;
    $data['total_items'] = count($count);

    echo json_encode($data);
}


}