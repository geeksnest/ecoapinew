<?php
namespace Controllers;

use \Models\Featuredprojects as Featuredprojects;
use \Models\Featuredphoto as Featuredphoto;
use \Models\Subscribers as Subscribers;
use \Models\Sentlogs as Sentlogs;
use \Models\Album as Album;
use \Models\Image as Image;
use \Models\Members as Members;

class SubscribersController extends \Phalcon\Mvc\Controller
{

    // add subscriber

    public function nmsaddAction(){
        $data = array();
        $NMSemail = trim($_POST['NMSemail']);

        $SubsEmail = Subscribers::findFirst("NMSemail='" . $NMSemail . "'");
        if ($SubsEmail == true) {
            ($SubsEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
        } else {
            $MemsEmail = Members::findFirst("email='" . $NMSemail . "'");
            if($MemsEmail == true){
                ($MemsEmail == true) ? $data["emailtaken2"] = "Email already a member." : '';
            }else{                        
                ($SubsEmail == false) ? $data["emailregister"] = "Thank you for subscribing to newsletter." : '';
                ($MemsEmail == false) ? $data["emailregister"] = "Thank you for subscribing to newsletter." : '';
                
                $submitdata = new Subscribers();
                $submitdata->assign(array(
                    'NMSemail'  => $NMSemail,
                    'NMSstat' => 1,
                    'NMSdate' => date('Y-m-d'),
                    'NMSstatTXT' => 'subscriber',
                ));
                if (!$submitdata->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                    echo json_encode(["error" => $submitdata->getMessages()]);
                }else{
                    $data = "Successfuly Save";
                }
            }
        }
        echo json_encode($data);
    }

    // subscribers list

    public function subscriberslistAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Subscribers::find(array("order" => "NMSstatTXT DESC"));
        } else {
            $conditions = "NMSemail LIKE '%" . $keyword . "%'";
            $Pages = Subscribers::find(array($conditions));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'NMSid' => $m->NMSid,
                'NMSemail' => $m->NMSemail,
                'NMSstat' => $m->NMSstat,
                'NMSdate' => $m->NMSdate,
                'NMSstatTXT' => $m->NMSstatTXT
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array(
            'data' => $data, 
            'pages' => $p, 
            'index' => $page->current, 
            'before' => $page->before, 
            'next' => $page->next, 
            'last' => $page->last, 
            'total_items' => $page->total_items
            ));
    }

    public function subscribersdeleteAction($pageid) {
        $conditions = "NMSid=" . $pageid;
        $page = Subscribers::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if($page->delete()){
                $data = array('success' => 'Subscriber Deleted');
            }
        }
        echo json_encode($data);
    }

    public function addsubscriberAction(){
        $data = array();
        if ($_POST) {
            $SubsEmail = Subscribers::findFirst("NMSemail='" . $_POST['NMSemail'] . "'");            
            if ($SubsEmail == true) {
                ($SubsEmail == true) ? $data["emailtaken"] = "Email already taken." : '';
            } else {
                
                $MemsEmail = Members::findFirst("email='" . $_POST['NMSemail'] . "'");

                if($MemsEmail == true){
                    ($MemsEmail == true) ? $data["emailtaken"] = "Email already a member." : '';
                }else{
                    $submitdata = new Subscribers();
                    $submitdata->assign(array(
                        'NMSemail'  => $_POST['NMSemail'],
                        'NMSstat' => 1,
                        'NMSdate' => date('Y-m-d'),
                        'NMSstatTXT' => 'subscriber',
                            ));
                    if (!$submitdata->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    }else{
                        $data = "Successfuly Save";
                    }
                }
            }
        }    
        echo json_encode($data);
    }

    public function subscriberinfoAction($pageid) {

        $subscriber = Subscribers::findFirst("NMSid=" . $pageid);
        $data = array();
        if ($subscriber) {
            $data = array(
                'pageid' => $subscriber->NMSid,
                'NMSemail' => $subscriber->NMSemail,
                'NMSstat' => $subscriber->NMSstat
                );
        }
        echo json_encode($data);
    }

    public function updatesubscriberAction(){
        $data = array();
        if ($_POST){
            
            if($_POST['NMSstat']){
                $NMSstat = 1;
                echo $NMSstatTXT = 'subscriber';
            }else{
                $NMSstat = 0;
                $NMSstatTXT = 'non-subscriber';
            }

            $pageid = $_POST['pageid'];
            
            $updsubs = Subscribers::findFirst('NMSid != '. $pageid .' and NMSemail="'. $_POST['NMSemail'] .'"');

            if($updsubs == true){
                ($updsubs == true) ? $data["alerts"] = "Err: Duplicate email found." : '';
            }else{    
                $updmem = Members::findFirst('email="'. $_POST['NMSemail'] .'"');

                if($updmem == true){
                    ($updmem == true) ? $data["alerts"] = "Err: Email found on members." : '';
                }else{        
                    $subscriber = Subscribers::findFirst('NMSid='.$pageid);
                    $subscriber->NMSemail = $_POST['NMSemail'];
                    $subscriber->NMSstat = $NMSstat;
                    $subscriber->NMSdate = date('Y-m-d');
                    $subscriber->NMSstatTXT = $NMSstatTXT;
                    if (!$subscriber->save()) {
                        $data['error'] = "Something went wrong saving the data, please try again.";
                    } else {
                        $data['success'] = "Success";
                    }
                }
            }
        }
        echo json_encode($data);
    }


    public function sentlistAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $conditions = "email like '%" . $keyword . "%'";
            $Pages = Sentlogs::find(array($conditions));
        } else {
            $conditions = "email like '%" . $keyword . "%'";
            $Pages = Sentlogs::find(array($conditions));
        }
        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'newslettertitle' => $m->newslettertitle
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array(
            'sentdata' => $data, 
            'pages' => $p, 
            'index' => $page->current, 
            'before' => $page->before, 
            'next' => $page->next, 
            'last' => $page->last, 
            'total_items' => $page->total_items
            ));
    }
}