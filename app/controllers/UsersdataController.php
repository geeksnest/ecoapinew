<?php

namespace Controllers;

use \Models\Members as Members;
use \Models\Membersotherinfo as Membersotherinfo;
use \Models\Projects as Projects;
use \Models\Projectauthor as Projectauthor;
use \Models\Projectsocialmedia as Projectsocialmedia;
use \Models\Projectsdonations as Projectsdonations;
use \Models\Notifications as Notifications;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class UsersdataController extends \Phalcon\Mvc\Controller
{	
	//get user data
	public function myprofileAction($userid){

		$app = new CB();
		$conditions = "SELECT * FROM members ";
		$conditions .= "LEFT JOIN membersotherinfo ON members.userid = membersotherinfo.userid ";
		$conditions .= "WHERE members.userid LIKE '".$userid."'";
		$searchresult = $app->dbSelect($conditions);
		
		echo json_encode($searchresult);
	}
	// update profile
	public function updateprofileAction($userid){

		if($_POST){
			$member = Members::findFirst("userid='" . $userid ."'");
			$member->username = $_POST['username'];               
			$member->email = $_POST['email'];
			$member->firstname = $_POST['firstname'];
			$member->lastname = $_POST['lastname'];               
			$member->birthday = $_POST['birthday'];
			$member->gender = $_POST['gender'];
			$member->location = $_POST['location'];
			$member->zipcode = $_POST['zipcode'];
			if (!$member->save()) {
				$errors = array();
				foreach ($member->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
			}else{
				$otherinfo = Membersotherinfo::findFirst("userid='" . $userid ."'");
				if($otherinfo){
					$otherinfo->aboutme = $_POST['aboutme'];
					$otherinfo->date_updated = date("Y-m-d H:i:s");
					if($_POST['photochange'] == 1){
						$otherinfo->photo = $_POST['photo']; 
					}
					if (!$otherinfo->save()) {
						$errors = array();
						foreach ($otherinfo->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array('error' => $errors));
					} else {
						$data['success'] = "Your profile has been updated";
					}
				}else{
					$otherinfo = new Membersotherinfo();

					$otherinfo->assign(array(
						'userid' => $userid,
						'aboutme' => $_POST['aboutme'],
						'photo' => $_POST['photo'],
						'date_updated' => date("Y-m-d H:i:s")
						));

					if (!$otherinfo->save()){
						$errors = array();
						foreach ($otherinfo->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						echo json_encode(array('error3' => $errors));
					}
					else {
						$data['success3'] = "Members other info saved.";
					}
				}			              
				
			}

			echo json_encode($data);
		}
		
	}

  // CHECK Username
	public function chkUsernameAction() {

		$userName = Members::findFirst("userid != '".$_POST['userid']."' AND username='" . $_POST['username'] . "'");
		if ($userName) {
			die(json_encode(array('unavailable' => 'Username unavailable.')));
		}else{
			$data['success'] = 'Username is available.';		
		}
		echo json_encode($data);
	}

  // CHECK Email
	public function chkEmailAction() {

		$userEmail = Members::findFirst("userid != '".$_POST['userid']."' AND email='" . $_POST['email'] . "'");
		if ($userEmail) {
			die(json_encode(array('unavailable' => 'Email address unavailable.')));
		}else{
			$data['success'] = 'Email address is available.';
		}
		echo json_encode($data);
	}

	// CHANGE PASSWORD
	public function changePasswordAction($userid){
		$data = array();
		if ($_POST){

			$currentpassword = sha1($_POST['currentpassword']);
			$newpassword = sha1($_POST['newpassword']);
			
			$user = Members::findFirst('userid="'.$userid.'" AND password="'.$currentpassword.'"');
			if($user){
				$user->password = $newpassword;
				if (!$user->save()) {
					$data['error'] = "Something went wrong saving the data, please try again.";
				} else {
					$data['success'] = "Congratiolations your password has been changed.";
				}
			}else{
				die(json_encode(array('incorrectpass' => 'Password incorrect.')));
			}
			

		}
		echo json_encode($data);
	}

	// get new notifications
	public function memberNotificationAction($userid){

		$app = new CB();
		$conditions = "SELECT ";
		$conditions .= "notifications.userID,notifications.readStatus,notifications.date_created,";
		$conditions .= "notifications.timeStarted,notifications.content,notifications.noteType,notifications.noteID,";
		$conditions .= "projects.projID,projects.projSlugs, ";
		$conditions .= "medialibrary.id,medialibrary.slugs ";
		$conditions .= "FROM notifications ";
		$conditions .= "LEFT JOIN projects on notifications.itemID=projects.projID ";
		$conditions .= "LEFT JOIN medialibrary on notifications.itemID=medialibrary.id ";
		$conditions .= "WHERE notifications.userID LIKE '".$userid."' AND notifications.readStatus=0 ";
		$conditions .= "ORDER BY notifications.num DESC";
		$notify = $app->dbSelect($conditions);

		echo json_encode($notify);
	}

	// get all notifications
	public function memberAllNotificationAction($userid,$offset, $num, $keyword){

		$app = new CB();
		$offsetfinal = ($offset * 10) - 10;
		$conditions = "SELECT ";
		$conditions .= "notifications.userID,notifications.readStatus,notifications.date_created,";
		$conditions .= "notifications.timeStarted,notifications.content,notifications.noteType,notifications.noteID,";
		$conditions .= "projects.projID,projects.projSlugs, ";
		$conditions .= "medialibrary.id,medialibrary.slugs ";
		$conditions .= "FROM notifications ";
		$conditions .= "LEFT JOIN projects on notifications.itemID=projects.projID ";
		$conditions .= "LEFT JOIN medialibrary on notifications.itemID=medialibrary.id ";
		if($keyword == 'undefined'){
		$conditions .= "WHERE notifications.userID LIKE '".$userid."'";}
		else{
		$conditions .= "WHERE notifications.userID LIKE '".$userid."' AND notifications.readStatus=$keyword";}


		$limit = " ORDER BY notifications.num DESC LIMIT ".$offsetfinal.", ". 10; 
		$notify = $app->dbSelect($conditions.$limit);
		$count = $app->dbSelect($conditions);

		echo json_encode(array('data' => $notify, 'total_items' => count($count),'index'=> $offset));
	}

	// get mark notifications if read
	public function markNotificationAction($noteID,$mark){

		$note = Notifications::findFirst("noteID='" . $noteID ."'");
		if($note){
			$note->readStatus = $mark;
			if (!$note->save()) {
				$errors = array();
				foreach ($note->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['error'] = $errors;
			} else {
				$data['success'] = $mark;
			}
		}
		echo json_encode($data);
	}



}

