<?php
namespace Controllers;
use \Models\Pages as Pages;
use \Models\Menumain as Menumain;
use \Models\Menusub as Menusub;
class MenuCreatorController extends \Phalcon\Mvc\Controller
{
    public function getPageAction() {
        $pages= Pages::find(array("menu_page != 1 ","order" => "title ASC"));
        foreach ($pages as $details) {
            $pageinfo[] = array(
                'pageid'  => $details->pageid,
                'title'   => $details->title
                );
        }
        echo json_encode($pageinfo);
    }

    public function saveAction() {
      
        if($_POST['type']==1){
              $pageupdate = Pages::findFirst('pageid='.$_POST['page'].'');
              $pageupdate->menu_page = "1";
              if(!$pageupdate->save()){
                echo json_encode(["error" => $saveData->getMessages()]);
            }
            //////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////
            $saveData = new Menumain();
            $saveData->assign(array(
                'menu_name'  => $_POST['name'],
                'page_id' => $_POST['page']
                ));
            if (!$saveData->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $saveData->getMessages()]);
            }
        }
        elseif($_POST['type']==0){
            $saveData = new Menusub();
            $saveData->assign(array(
                'sub_name'  => $_POST['name'],
                'page_id' => $_POST['page'],
                'main_id' => $_POST['menuMain']
                ));
            if (!$saveData->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
                echo json_encode(["error" => $saveData->getMessages()]);
            }
        }
    }

    public function listmenuAction() {
        // $menus= Menumain::find();
        // foreach ($menus as $details) {
        //     $submenus= Menusub::find(array("main_id" =>"'".$details->id."'" ));
        //     $listmenu = json_encode($submenus->toArray(), JSON_NUMERIC_CHECK);
        //     $menulist[] = array(
        //         'menu_id' => $details->id,
        //         'menu_name' => $details->menu_name,
        //         'sub_menu'=>$listmenu
        //         );
        // }
        // echo json_encode($menulist);

        $menus= Menumain::find();
        foreach ($menus as $details) {
            $submenus= Menusub::find(array("main_id" =>"'".$details->id."'" ));
            $listmenu = json_encode($submenus->toArray(), JSON_NUMERIC_CHECK);
            $menulist[] = array(
                'menu_id' => $details->id,
                'menu_name' => $details->menu_name,
                'page_id' => $details->page_id
                );
        }
        echo $showlist= json_encode($menulist);

    }

    public function submenuAction() {
      

        $menu= Menusub::find();
        foreach ($menu as $detail) {
            $submenu[] = array(
                'sub_id' => $detail->main_id,
                'menu_name' => $detail->sub_name
                );
        }
        echo json_encode($submenu);

    }




}

