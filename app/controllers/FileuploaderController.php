<?php
namespace Controllers;
use \Models\Image as Image;
use \Models\Album  as Album;
class FileuploaderController extends \Phalcon\Mvc\Controller
{
    public function slideruploadAction()
    {
        var_dump($_POST);
        
    }
    /*
    * Move File Upload of Programs 
    */
    public function ajaxfileuploaderAction($filename, $folderName,$folderid){
        // $newpicname = 0;
        $sort = 0;
        mkdir("images/".$folderName."/thumbnail", 0777, TRUE);
        
        $getType=explode('.', $filename);
        $newfileName = trim(md5(uniqid(rand(), true)).'.'.$getType[1]);// New Image Name
        $generateid=md5(uniqid(rand(), true));

        if(is_file('../public/server/php/files/'.$filename)){
            rename('../public/server/php/files/'.$filename, '../public/images/'.$folderName.'/'.$newfileName);
        }
        if(is_file('../public/server/php/files/thumbnail/'.$filename)){
            rename('../public/server/php/files/thumbnail/'.$filename, '../public/images/'.$folderName.'/thumbnail/'.$newfileName);
        }
         $imgUpload = new Image();
         $imgUpload->assign(array(
                    'generateid'=>$generateid,
                    'description'=>'Description Here',
                    'path'=>$newfileName,
                    'title'=>'Title Here',
                    'foldername'=>$folderName,
                    'folderid'=>$folderid,
                    'sort'=>$sort
                ));
        if (!$imgUpload->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
            echo json_encode(["error" => $imgUpload->getMessages()]);
        }else{
            $getImage= Image::find(array("order" => "id DESC"));
            foreach ($getImage as $getImages) {;
                $data[] = array(
                    'imgid'=>$getImages->id,
                    'description'=>$getImages->description,
                    'imgpath'=>$getImages->path,
                    'imgtitle'=>$getImages->title
                );
            }
            echo json_encode($data);
        }
    }

    /*
    * Display Content
    */
    public function imagelistAction($getid){

        $getImage= Image::find(array("folderid='".$getid."'","order" => "id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'imgid'         => $getImages->id,
                'description'   => $getImages->description,
                'imgpath'       => $getImages->path,
                'imgtitle'      => $getImages->title,
                'foldername'    => $getImages->foldername,
                'linkname'      => $getImages->linkname,
                'linkpath'      => $getImages->linkpath,
                'sort'          => $getImages->sort
            );
        }
        echo json_encode($data);

    }
     public function updateinfoimgAction(){
        $id = $_POST['id'];
        $sliderimages = Image::findFirst('id='.$id.' ');
        $sliderimages->description = $_POST['description'];
        $sliderimages->title= $_POST['title'];
        $sliderimages->linkname= $_POST['linkname'];
        $sliderimages->linkpath= $_POST['linkpath'];
        $sliderimages->sort= $_POST['sort'];
        if(!$sliderimages->save()){
            echo 'Error';
        }else{

        }
    }
    public function dltphotoAction(){
        $id = $_POST['id'];
        $dltPhoto = Image::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }
    public function folderidAction(){
        $generateid=md5(uniqid(rand(), true));
        $genId=array('folderid'=>$generateid);
        echo json_encode($genId);
    }
    public function createalbumAction($albumname,$albumid){
        // $albuminfo= $albumname."--".$albumid;
        // echo json_encode($albuminfo);
        $countexist= Album::find(array("album_id='".$albumid."'"));
        if(count($countexist)==0)
        {
            $createAlbum = new Album();
            $createAlbum->assign(array(
                'album_name'=>$albumname,
                'album_id'=>$albumid,
                'main'=>0
            ));
              if (!$createAlbum->save()) {
                        $albuminfo['error'] = "Something went wrong saving the data, please try again.";
                        echo json_encode(["error" => $albuminfo->getMessages()]);
            }
        }
    }

    public function mainsetAction($folderid) {
        $mainsets = Album::findFirst('main=1');
        if($mainsets){
            $mainsets->main = 0;
            if(!$mainsets->save()){
                $data="error";
            }else{
                $data="Success";
            }
        }
    
        $mainset = Album::findFirst("album_id='".$folderid."'");
        if($mainset){
            $mainset->main = 1;    
            if(!$mainset->save()){
                $data="error";
            }else{
                $data="Success";
            }
        }
        
        echo json_encode($data);
    }

    public function mainslideAction() {

        $album = Album::findFirst('main=1');
        $data = array();
        if ($album) {
            $image = Image::find(array("folderid='".$album->album_id."'", "order" => "sort ASC"));
            $slides = json_encode($image->toArray(), JSON_NUMERIC_CHECK);
            $data = array(
                'slides' => $slides
                );
        }
        echo json_encode($data);
    }
}

