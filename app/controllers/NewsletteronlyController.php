<?php

namespace Controllers;

use \Models\Members as Members;
use \Models\Membersotherinfo as Membersotherinfo;
use \Models\Pledgeonly as Pledgeonly;
use \Models\Notifications as Notifications;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use PHPMailer as PHPMailer;
use MailChimp as MailChimp;

class NewsletteronlyController extends \Phalcon\Mvc\Controller
{	
	
	// record who join the pledge
	public function joinPledgeAction(){

		if($_POST){
			$guid = new \Utilities\Guid\Guid();
			$pledgeID = $guid->GUID();

			$pledge = new Pledgeonly();
			$pledge->pledgeID = $pledgeID;               
			$pledge->email = $_POST['email'];
			$pledge->fname = $_POST['fname'];
			$pledge->lname = $_POST['lname'];               
			$pledge->date_created = date("Y-m-d H:i:s");
			$pledge->date_updated = date("Y-m-d H:i:s");
			if (!$pledge->save()) {
				$errors = array();
				foreach ($pledge->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				echo json_encode(array('error' => $errors));
			} else {

				$MailChimp = new MailChimp('289ce16d32317e58af6e65afe7a6c538-us10');
				$result = $MailChimp->call('lists/subscribe', array(
					'id'                => 'fc51891e6f',
					'email'             => array('email'=>$_POST['email']),
					'merge_vars'        => array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname']),
					'double_optin'      => false,
					'update_existing'   => true,
					'replace_interests' => false,
					'send_welcome'      => false,
					));		

				$data['success'] = "Pledge successfuly added.";	              
				$data['sent'] = "ok";	              
				$data['mailchimp'] = $result;	              
				
			}

			echo json_encode($data);
		}
		
	}





}

