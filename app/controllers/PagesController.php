<?php

namespace Controllers;

use \Models\Donationlog as Donationlog;
use \Models\Donation as Donation;
use \Models\Memberconfirmation as Memberconfirmation;
use PHPMailer as PHPMailer;
use MailChimp as MailChimp;
use \Models\Pages as Pages;
use \Models\Atw as Atw;
use \Controllers\ControllerBase as CB;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPageAction() {
        $data = array();
        if ($_POST) {
            $page = new Pages();
            $page->assign(array(
                'title' => $_POST['title'],
                'pageslugs' => strtolower($_POST['slugs']),
                'body' => $_POST['body'],
                'banner' => $_POST['banner'],
                'bannertitle' => $_POST['bannertitle'],
                'titlefontsize' => $_POST['titlefontsize'],
                'bannertext' => $_POST['bannertext'],
                'descriptionfontsize' => $_POST['descriptionfontsize'],
                'metatitle' => $_POST['metatitle'],
                'metadescription' => $_POST['metadescription'],
                'metakeyword' => $_POST['metakeyword'],
                'btnname' => $_POST['btnname'],
                'btnlink' => $_POST['btnlink'],
                'btncolor' => $_POST['btncolor'],
                'banneropt' => $_POST['banneropt'],
                'status' => 1,
                'menutitle' => $_POST['menutitle'],
                'datecreated' => date('Y-m-d')
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
                $data['error'] = "Something went wrong saving the data, please try again.";
            } else {
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function checkpagetitlesAction($pagetitle) {
        $data = array();
        if($pagetitle){

            $newStr = trim($pagetitle, '"');
            $title = str_replace('\"', '', $newStr);  
            $pagetitle = Pages::findFirst('title LIKE "' . $title . '"');

            if ($pagetitle == true) {
                ($pagetitle == true) ? $data["pagealreadyexist"] = "The page title is already exist!" : '';
            }else{
                ($pagetitle == false) ? $data["pageareavailable"] = "The page title are available." : '';            
            }

            echo json_encode($data);
        }
    }

    public function managepagesAction($num, $page, $keyword, $sort, $sortto) {

        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if ($keyword == 'null' || $keyword == 'undefined') {

            $conditions = "SELECT * FROM pages ";

        } else {

            $conditions = "SELECT * FROM pages WHERE title LIKE '%". $keyword ."%' ";
            
        }

        if($sortto == 'DESC'){
            $sortby = "ORDER BY $sort DESC";
        }else{
            $sortby = "ORDER BY $sort ASC";
        }

        $conditions .= $sortby;

        $searchresult = $app->dbSelect($conditions. " LIMIT " . $offsetfinal . ",10");

        $count = $app->dbSelect($conditions);

        echo json_encode(array(
            'data' => $searchresult,
            'index' => $page,
            'total_items' => count($count)
            )); 


    }

    public function pageinfoAction($pageid) {

        $pages = Pages::findFirst("pageid=" . $pageid);
        $data = array();
        if ($pages) {
            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'slugs' => htmlentities($pages->pageslugs),
                'body' => $pages->body,
                'banner' => $pages->banner,
                'bannertitle' => $pages->bannertitle,
                'titlefontsize' => $pages->titlefontsize,
                'bannertext' => $pages->bannertext,
                'descriptionfontsize' => $pages->descriptionfontsize,
                'status' => $pages->status,
                'menutitle' => $pages->menutitle,
                'metatitle' => $pages->metatitle,
                'metadescription' => $pages->metadescription,
                'metakeyword' => $pages->metakeyword,
                'pagefeatures' => $pages->pagefeatures,
                'btnname' => $pages->btnname,
                'btnlink' => $pages->btnlink,
                'banneropt' => $pages->banneropt,
                );
        }
        echo json_encode($data);
    }

    public function pageUpdateAction() {

        $data = array();
        if ($_POST) { 

            $pageid = $_POST['pageid'];
            $pagetitle = $_POST['title'];
            $newStr = trim($pagetitle, '"');
            $title2 = str_replace('\"', '', $newStr);  

            $titlepage = Pages::findFirst('title LIKE "' . $title2 . '"');
            $idpage = Pages::findFirst('title LIKE "' . $title2 . '" AND pageid= ' . $pageid);

            if ($titlepage == true && $idpage == false ) {
                ($titlepage == true) ? $data["pagealreadyexist"] = "The page title is already exist!" : '';
            }else{

                if ($_POST['status'] == "true") {
                    $status = 1;
                } else {
                    $status = 0;
                }
                
                $page = Pages::findFirst('pageid=' . $pageid . ' ');
                $page->title = $pagetitle;
                $page->pageslugs = strtolower($_POST['slugs']);
                $page->body = $_POST['body'];
                $page->banner = $_POST['banner'];
                $page->bannertitle = $_POST['bannertitle'];
                $page->titlefontsize =$_POST['titlefontsize'];
                $page->bannertext = $_POST['bannertext'];
                $page->descriptionfontsize = $_POST['descriptionfontsize'];
                $page->metatitle = $_POST['metatitle'];
                $page->metadescription = $_POST['metadescription'];
                $page->metakeyword = $_POST['metakeyword'];
                $page->status = $status;
                $page->menutitle = $_POST['menutitle'];
                $page->pagefeatures = $_POST['pagefeatures'];
                $page->btnname = $_POST['btnname'];
                $page->btnlink = $_POST['btnlink'];
                $page->banneropt = $_POST['banneropt'];

                if (!$page->save()) {
                    $data['error'] = "Something went wrong saving the data, please try again.";
                } else {
                    $data['success'] = "Success";
                } 
            }        
        }
        echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = "pageid=" . $pageid;
        $page = Pages::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($page) {
            if ($page->delete()) {
                $data = array('success' => 'Page Deleted');
            }
        }
        echo json_encode($data);
    }

    public function getPageAction($pageslugs) {
        $conditions = "pageslugs LIKE'" . $pageslugs . "' AND status=1" ;
        echo json_encode(Pages::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);

    }

    public function getPageprojectAction() {
        echo json_encode(Pages::findFirst(array())->toArray(), JSON_NUMERIC_CHECK);
    }


    public function updatepagestatusAction($status,$pageid) {

        $data = array();
        if ($status == 1){
            $stat = 0;
        }else{
            $stat = 1;
        }
        $conditions = 'pageid="'.$pageid.'"';
        $page = Pages::findFirst($conditions);
        $page->status = $stat;
        if (!$page->save()) {
            $data['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
        }

        echo json_encode($data);
    }

}
