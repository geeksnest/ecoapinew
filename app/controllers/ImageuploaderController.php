<?php
namespace Controllers;
use \Models\Pageimage as Pageimage;
class ImageuploaderController extends \Phalcon\Mvc\Controller
{
    
    public function ajaxfileuploaderAction($filename, $type){        

        $filename = $_POST['imgfilename'];
        $picture = new Pageimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data); 
       
    }   
    
    public function imagelistAction(){

        $getImage= Pageimage::find(array("order" => "id DESC"));
        foreach ($getImage as $getImages) {;
            $data[] = array(
                'id'   => $getImages->id,
                'filename'   => $getImages->filename
                );
        }
        echo json_encode($data);	
    }

     public function updateinfoimgAction(){
        $id = $_POST['id'];
        $sliderimages = Pageimage::findFirst('id='.$id.' ');
        $sliderimages->description = $_POST['description'];
        $sliderimages->title= $_POST['title'];
        if(!$sliderimages->save()){
            echo 'Error';
        }else{

        }
    }

    public function dltphotoAction(){
        $id = $_POST['id'];
        $dltPhoto = Pageimage::findFirst('id='.$id.' ');
        $data = array('error' => 'Not Found');
        if ($dltPhoto) {
            if($dltPhoto->delete()){
                $data = array('success' => 'Photo has Been deleted');
            }
        }
        echo json_encode($data);
    }

}

